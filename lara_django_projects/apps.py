"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_projects app *

:details: lara_django_projects app configuration. 
         This provides a generic django app configuration mechanism.
         For more details see:
         https://docs.djangoproject.com/en/4.0/ref/applications/
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - 
________________________________________________________________________
"""


from django.apps import AppConfig


class LaraDjangoProjectsConfig(AppConfig):
    name = 'lara_django_projects'
    # enter a verbose name for your app: lara_django_projects here - this will be used in the admin interface
    verbose_name = 'LARA-django Projects'
    lara_app_icon = 'lara_projects_icon.svg'  # this will be used to display an icon, e.g. in the main LARA menu.
