"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_projects views *

:details: lara_django_projects views module.
         - add app specific urls here
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: -
________________________________________________________________________
"""

from dataclasses import dataclass, field
from typing import List

from django.shortcuts import get_object_or_404, render, redirect
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse

from django.views.generic import DetailView, ListView, CreateView, UpdateView, DeleteView

from django_tables2 import SingleTableView

from .models import Project, Experiment

from .forms import ProjectCreateForm, ProjectUpdateForm, ExperimentCreateForm, ExperimentUpdateForm
from .tables import ProjectTable, ExperimentTable


# Create your  lara_django_projects views here.


@dataclass
class ProjectMenu:
    menu_items:  List[dict] = field(default_factory=lambda: [   
        {'name': 'Projects Overview',
         'path': 'lara_django_projects:project-overview'},
        {'name': 'Projects Table',
         'path': 'lara_django_projects:project-list'},
        {'name': 'Experiments',
                 'path': 'lara_django_projects:experiment-list'}
    ])

    
class ProjectSingleTableView(SingleTableView):
    model = Project
    table_class = ProjectTable

    #fields = ('name', 'name_full', 'URL', 'handle', 'IRI', 'manufacturer', 'model_no', 'product_type', 'type_barcode', 'product_no', 'weight', 'specifications', 'spec_JSON', 'spec_doc', 'brochure', 'quickstart', 'manual', 'service_manual', 'icon', 'image', 'description', 'project_id', 'project_class', 'shape')

    template_name = 'lara_django_projects/list.html'
    success_url = '/projects/project/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Project - List"
        context['create_link'] = 'lara_django_projects:project-create'
        context['menu_items'] = ProjectMenu().menu_items
        return context


class ProjectDetailView(DetailView):
    model = Project

    template_name = 'lara_django_projects/project_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Project - Details"
        context['update_link'] = 'lara_django_projects:project-update'
        context['menu_items'] = ProjectMenu().menu_items
        return context


class ProjectCreateView(CreateView):
    model = Project

    template_name = 'lara_django_projects/create_form.html'
    form_class = ProjectCreateForm
    success_url = '/projects/project/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Project - Create"
        return context


class ProjectUpdateView(UpdateView):
    model = Project

    template_name = 'lara_django_projects/update_form.html'
    form_class = ProjectUpdateForm
    success_url = '/projects/project/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Project - Update"
        context['delete_link'] = 'lara_django_projects:project-delete'
        return context


class ProjectDeleteView(DeleteView):
    model = Project

    template_name = 'lara_django_projects/delete_form.html'
    success_url = '/projects/project/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Project - Delete"
        context['delete_link'] = 'lara_django_projects:project-delete'
        return context



class ExperimentSingleTableView(SingleTableView):
    model = Experiment
    table_class = ExperimentTable

    #fields = ('name', 'name_full', 'URL', 'handle', 'IRI', 'manufacturer', 'model_no', 'product_type', 'type_barcode', 'product_no', 'weight', 'specifications', 'spec_JSON', 'spec_doc', 'brochure', 'quickstart', 'manual', 'service_manual', 'icon', 'image', 'description', 'experiment_id', 'experiment_class', 'shape')

    template_name = 'lara_django_projects/list.html'
    success_url = '/projects/experiment/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Experiment - List"
        context['create_link'] = 'lara_django_projects:experiment-create'
        context['menu_items'] = ProjectMenu().menu_items
        return context


class ExperimentDetailView(DetailView):
    model = Experiment

    template_name = 'lara_django_projects/experiment_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Experiment - Details"
        context['update_link'] = 'lara_django_projects:experiment-update'
        context['menu_items'] = ProjectMenu().menu_items
        return context


class ExperimentCreateView(CreateView):
    model = Experiment

    template_name = 'lara_django_projects/create_form.html'
    form_class = ExperimentCreateForm
    success_url = '/projects/experiment/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Experiment - Create"
        return context


class ExperimentUpdateView(UpdateView):
    model = Experiment

    template_name = 'lara_django_projects/update_form.html'
    form_class = ExperimentUpdateForm
    success_url = '/projects/experiment/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Experiment - Update"
        context['delete_link'] = 'lara_django_projects:experiment-delete'
        return context


class ExperimentDeleteView(DeleteView):
    model = Experiment

    template_name = 'lara_django_projects/delete_form.html'
    success_url = '/projects/experiment/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Experiment - Delete"
        context['delete_link'] = 'lara_django_projects:experiment-delete'
        return context

# --- old


class RootIndexView(ListView):
    model = Project
    paginate_by = 100
    template_name = 'lara_django_projects/root_index.html'
    #context_object_name = 'root_projects'

    def get_queryset(self):
        """Return only root projects."""
        return Project.objects.filter(level=1)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Project - Overview"
        context['create_link'] = 'lara_django_projects:project-create'
        context['menu_items'] = ProjectMenu().menu_items
        context['root_projects'] = self.get_queryset()
        return context


class ProjectTree(ListView):
    """ Class doc """
    model = Project
    template_name = 'lara_django_projects/project_list.html'
    context_object_name = 'project_list'
    paginate_by = 100

    def get_queryset(self,  *args, **kwargs):
        """Return only projects under selected project"""
        print("kwargs: ", self.kwargs)
        self.subtree_root_project = Project.objects.get(project_id=self.kwargs['pk'])
        print(self.subtree_root_project.title)

        return self.subtree_root_project.get_descendants()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        
        context['section_title'] = "Project - Tree"
        context['create_link'] = 'lara_django_projects:project-create'
        context['menu_items'] = ProjectMenu().menu_items
        context['subtree_root'] = self.subtree_root_project
        print("context: ", context)
        return context


class ProjectDetails(DetailView):
    model = Project


class ExperimentDetails(DetailView):
    model = Experiment


def show_proj_tree(request):
    context = {'main_project': "All",
               'projects': Project.objects.all()}
    return render(request, "lara_projects/proj_tree.html", context)


# --------- to be reviewed :

def searchForm(request):
    return render(request, 'lara_projects/search.html', context={})


def search(request):
    search_string = request.POST['search']

    try:
        project_list = []
        curr_project = Project.objects.get(project__name=search_string)
        project_list.append(curr_project)

    except ValueError as err:
        sys.stder.write("{}".format(err))
        # ~ return render(request, 'polls/detail.html', {
        # ~ 'question': question,
        # ~ 'error_message': "You didn't select a choice.",
        # ~ })
    else:
        context = {'project_list': project_list}
        return render(request, 'lara_projects/results.html', context)
        # return HttpResponseRedirect(reverse('lara_projects:results', args=(lara_device_list,)))


def searchResults(request, project_list):
    #question = get_object_or_404(Question, pk=question_id)
    context = {'project_list': project_list}
    return render(request, 'lara_projects/results.html', context)
