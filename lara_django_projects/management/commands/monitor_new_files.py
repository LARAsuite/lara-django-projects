"""_____________________________________________________________________

:PROJECT: LARA

*lara_projects monitor new files *

:details: lara_projects database models. 
         -

:file:    monitor_new_files.py
:authors: mark doerr (mark.doerr@uni-greifswald.de)

:date: (creation)          20161122
:date: (last modification) 20190127

.. note:: -
.. todo:: - 
________________________________________________________________________
**Copyright**:
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

  For further Information see LICENSE file that comes with this distribution.
________________________________________________________________________
"""

__version__ = "0.1.5"

import os
import pyinotify
import csv
import re
import logging

from django.conf import settings
from django.core.management.base import BaseCommand, CommandError

from laralib.projects.add_projects_python import  AddPythonProjects2DB
from laralib.projects.add_projects_json import  AddJSONProjects2DB
from laralib.container.add_layouts import  AddLayouts2DB

from lara_data.eval_data import EvalDataDB

class Command(BaseCommand):
    #~ args = ''
    help = 'watching for new files in dir:\n%s now   (s. settings.DJANGO_TO_MONITOR_PATH )....\n!! Please copy and do not move files !! \n(press CTRL-C to interrupt)\n' % settings.DJANGO_TO_MONITOR_PATH
    
    logging.basicConfig(format='%(levelname)s| %(module)s.%(funcName)s:%(message)s', level=logging.DEBUG)
    #~ logging.basicConfig(format='%(levelname)s| %(module)s.%(funcName)s:%(message)s', level=logging.ERROR)
    
    def add_arguments(self, parser):
        """ command line arguments s. https://docs.python.org/2/library/argparse.html#module-argparse """
        parser.add_argument('-e','--evaluate-data',
            action='store',
            dest='eval_data',
            default=False,
            help='Evaluate data on the fly')
        
    def handle(self, *args, **options):
        info_str = "\nGeneral procedure: \n1. generate and initialize database (before calling monitor_new_files) \n2. add container layouts \n3. add measurement data\n4. trigger evaluation"
        logging.info(info_str)
        
        wm = pyinotify.WatchManager()
        mask = pyinotify.IN_DELETE | pyinotify.IN_CREATE
        notifier = pyinotify.Notifier(wm, DataWatcher())
        
        wdd = wm.add_watch(settings.DJANGO_TO_MONITOR_PATH, mask, rec=True)
        
        self.stdout.write("watching for new files in dir:\n  %s now  ... \n   !!  Please copy and do not move files !! \n(press CTRL-C to interrupt)" % settings.DJANGO_TO_MONITOR_PATH)
        #~ self.stdout.write(self.help)
        while True:
          try:
            notifier.process_events()
            if notifier.check_events():
              notifier.read_events()
          except KeyboardInterrupt:
            notifier.stop()
            break
        exit()
        
class DataWatcher(pyinotify.ProcessEvent):
    """ Main dispatcher class for filesystem/inotify events """
    def process_IN_CREATE(self, event): # this is executed in case of creation
    
        self.file_to_processed_name = event.name
        self.file_to_processed_full_name = os.path.join(event.path, event.name)
        logging.info( "New copy of file %s arrived in monitor dir; processing ...." % self.file_to_processed_full_name)
        
        # parse filename
        filename_base, file_extension = os.path.splitext(self.file_to_processed_name)
        
        if file_extension == ".cvs" :
            pass
        
        # file name patterns for dispatching         
        plate_layout_pattern = r"^\w*_plate_layout"
        barcode_file_pattern = r"^QR_"
        evaluation_file_pattern = r"^evaluation_"
        canonical_pattern = r"BC_\w*_\d{8}_\d{6}_"
        
        if re.search(plate_layout_pattern, self.file_to_processed_name ):
          logging.info( "layout file %s found !" % self.file_to_processed_name)
          
          #~ layout_adder = AddLayouts2DB(event=event)
          layout_adder.cleanup()
          
        elif re.search(barcode_file_pattern, self.file_to_processed_name):
            #~ self.genExperimentFile()
            logging.debug("gen experiment file")
            
            #~ gen_exp = GenExperimetJSONfil()
            #~ gen_exp.cleanup()
        
        elif re.search(evaluation_file_pattern, self.file_to_processed_name):
            #~ self.evaluateExperiments()
            logging.debug("eval multi experiment file")
            
            evaluator = EvalDataDB(event=event)
            #~ evaluator.cleanup()
        
        # check, if file has a canonical filename: BC_barcode_YYYYMMDD_HHMMSS_
        elif re.search(canonical_pattern,self.file_to_processed_name ):
            logging.debug("adding data")
           
            #~ data_adder = AddData2DB(event=event)
            #~ data_adder.cleanup() # is called in constructor
    
    def process_IN_DELETE(self, event):
        logging.info( "file %s deleted !" % os.path.join(event.path, event.name) )
    
    def process_IN_MOVE(self, event):
        logging.info( "file %s moved - please copy file, do not move !" % os.path.join(event.path, event.name) )
    
