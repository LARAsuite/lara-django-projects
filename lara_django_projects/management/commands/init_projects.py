"""_____________________________________________________________________

:PROJECT: LARA

*lara_projects init_projects *

:details: lara_projects init_projects. 
         -

:file:    init_projects.py
:authors: mark doerr (mark.doerr@uni-greifswald.de)

:date: (creation)          20150601
:date: (last modification) 20190127

.. note:: -
.. todo:: - 
________________________________________________________________________
**Copyright**:
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

  For further Information see LICENSE file that comes with this distribution.
________________________________________________________________________
"""

__version__ = "0.1.5"

import logging

from django.core.management.base import BaseCommand, CommandError

from lara_django_projects.management.add_projects import AddProjects2DB
#~ from laralib.chem.add_substances import AddSubstances2DB

class Command(BaseCommand):
    """see https://docs.djangoproject.com/en/2.1/howto/custom-management-commands/ for more details
    using now new argparse mechanism of django > 1.8
    """    
    logging.basicConfig(format='%(levelname)s| %(module)s.%(funcName)s:%(message)s', level=logging.DEBUG)
    #~ logging.basicConfig(format='%(levelname)s| %(module)s.%(funcName)s:%(message)s', level=logging.ERROR)
    
    help = 'Initialize a database with default settings for projects.'
    
    def add_arguments(self, parser):
        """ command line arguments s. https://docs.python.org/2/library/argparse.html#module-argparse """
        
        # defining named commandline arguments
        parser.add_argument('-f','--project-file',
            action='store',
            dest='project_file',
            default="",
            help='python_proj_def file defining the project/experiment structure / hierachy, used for additon or replacement of database entries')
            
        parser.add_argument('-json','--json-project-file',
            action='store',
            dest='json_project_file',
            default="",
            help='defining the project JSON file defining the project/experiment structure / hierachy, used for additon or replacement of database entries')
            
        parser.add_argument('-coll','--project-csv-file',
            action='store',
            dest='project_csv_file',
            default="",
            help='collection of project files defining the project/experiment structure / hierachy, used for additon or replacement of database entries, in csv format')
            
        parser.add_argument('-subst','--substance-csv',
            action='store',
            dest='substance_csv',
            default="",
            help='substance csv file for additon or replacement of database entries')
            
        parser.add_argument('-prot','--protein-csv',
            action='store',
            dest='protein_csv',
            default="",
            help='substance csv file for additon or replacement of database entries')
        
        parser.add_argument('-seq','--sequence-csv',
            action='store',
            dest='sequence_csv',
            default="",
            help='sequence csv file for additon or replacement of database entries')
            
        parser.add_argument('-res','--results-csv',
            action='store',
            dest='results_csv',
            default="",
            help='results csv file for additon or replacement of database entries')
            
        parser.add_argument('-r','--replace',
            action='store_true',
            dest='replace_data',
            default=False,
            help='all old entries in the database will be replaced by updated versions from the JASON file')
            
        parser.add_argument('-c','--check',
            action='store_true',
            dest='check_file',
            default=False,
            help='check integrety of the project JSON file')
            
        parser.add_argument('-p','--pretty-print',
            action='store_true',
            dest='pp_file',
            default=False,
            help='Pretty print project JSON file')

    def handle(self, *args, **options):
        """Dispatcher based on commandline arguments/options"""
        
        if options['check_file'] :
            logging.debug("checking integrety of file {}".format(options['project_file']) )
            #~ AddProjects2DB( options['check_file'] ) 
           
        elif options['pp_file'] :
            logging.debug("pretty print file {}".format(options['project_file']) )
            #~ adpr2db = AddProjects2DB( options['pp_file'] ) 
            
            #~ adpr2db.prettyPrintJSON()

        elif options['project_csv_file'] != "":
            # multiple files see:
            # http://stackoverflow.com/questions/26727314/multiple-files-for-one-argument-in-argparse-python-2-7
            
            adpr2db = AddProjects2DB( filename=options['project_csv_file'], 
                                      replace_data=options['replace_data'] ) 
                                                  
        elif options['project_file'] != "":
            # multiple files see:
            # http://stackoverflow.com/questions/26727314/multiple-files-for-one-argument-in-argparse-python-2-7
            
            adpr2db = AddProjects2DB( filename=options['project_file'], 
                                      replace_data=options['replace_data'] ) 
            
        #~ elif options['substance_csv'] :
            #~ as2db = AddSubstances2DB( csv_filename=options['substance_csv'])
            
        #~ elif options['protein_csv'] :
            #~ AddProteins2DB( csv_filename=options['protein_csv'])
            
        #~ elif options['sequence_csv'] :
            #~ AddSequences2DB( csv_filename = options['sequence_csv'] )
