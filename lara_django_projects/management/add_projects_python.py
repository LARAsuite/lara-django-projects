"""_____________________________________________________________________

:PROJECT: LARA

*lara_projects models *

:details: lara_projects database models. 
         -

:file:    models.py
:authors: mark doerr (mark.doerr@uni-greifswald.de)

:date: (creation)          
:date: (last modification) 20191018

.. note:: -
.. todo:: - 
________________________________________________________________________
**Copyright**:
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

  For further Information see LICENSE file that comes with this distribution.
________________________________________________________________________
"""

__version__ = "0.1.9"

import os
import sys
import logging
import json
import csv
import re
import datetime
import weakref

from django.db import IntegrityError
from django.core.management.base import BaseCommand, CommandError
from django.core.exceptions import ObjectDoesNotExist
from django.utils import timezone

# ~ from laralib.projects.projects import ProjectAbstr, ExperimentAbstr

from lara_django_base.models import Namespace
from lara_django_people.models import Entity
from lara_django_containers.models import ContainerLayout, Container
from laralib.container.barcode_handler import expandBCsequence

from lara_django_devices.models import Device
from lara_django_data.models import Evaluation

from laralib.evaluation.safe_exec import safe_exec

from ..models import Project as LP_Project
from ..models import Experiment as LP_Experiment


class Project:
    """ Class doc

        For keeping track of tree leafs enables faster (recursive) addition of projects:
        if a parent already exists, then one does not need to check, if all parents exist...
    """
    __instances = set()
    __proj_leafes = set()
    __instance_list = []

    proj_non_parents = []

    # s. http://effbot.org/pyfaq/how-do-i-get-a-list-of-all-instances-of-a-given-class.htm
    @classmethod
    def addProjLeaf(proj, parent):
        if parent in __proj_leafes:
            __proj_leafes -= parent
        else:
            _proj_leafes.add(proj)

    @classmethod
    def getProjLeafes(cls):
        for leaf in __proj_leafes:
            yield leaf

    @classmethod
    def getInstancesFromList(cls):
        for ref in cls.__instance_list:
            yield ref()

    @classmethod
    def getInstances(cls):
        dead = set()
        for ref in cls.__instances:
            obj = ref()
            if obj is not None:
                yield obj
            else:
                dead.add(ref)
        cls.__instances -= dead

    def __init__(self,
                 parent=None,
                 parent_project_name="",
                 name=None,
                 title="",
                 description="",
                 users=["LAR"],
                 startDatetime="1969-06-09T12:21",
                 remarks="",
                 evaluation=None):
        """ :param parent: parent evalution
            :param parameters: evaluation parameters, a JSON string
        """

        self.parent = parent
        self.name = name
        self.title = title
        self.description = description
        self.users = users
        self.startDatetime_str = startDatetime
        self.remarks = remarks
        self.parent_project_name = parent_project_name
        self.evaluation = evaluation

        self.user_lst = []

        self.__instances.add(weakref.ref(self))
        self.__instance_list.append(weakref.ref(self))

    def add2DB(self, namespace):
        logging.debug(
            "~~~~~~~~~~ add proj db implementation {}".format(self.name))

        self.name_full = ".".join((namespace, self.name))

        #~ logging.debug("1 parent proj name: {}".format(self.parent_project_name) )
        # check, if parent exists
        if self.parent_project_name != "":
            self.parent_project_name_full = ".".join(
                (namespace, self.parent_project_name))
            parent_project = LP_Project.objects.get(
                name_full=self.parent_project_name_full)
        else:
            self.parent_project_name_full = ".".join(
                (namespace, self.parent.name))
            logging.debug("2 parent proj name: {}".format(
                self.parent_project_name_full))
            parent_project = LP_Project.objects.get(
                name_full=self.parent_project_name_full)

        if parent_project is None:
            sys.stderr.write(f"ERROR: No parent of {self.name} exists !!!\n")
            Project.proj_non_parents.append(self.parent_project_name_full)
            return

        # check namespace
        la_namespace = Namespace.objects.get(namespace=namespace)

        if la_namespace is None:
            sys.stderr.write(
                f"ERROR: Namespace [{namespace}] does not exist in this Database !!!\n")
            return

        # check, if project exists
        if not LP_Project.objects.filter(name=self.name).exists():
            # parent_it = Project.objects.get(name=)
            logging.debug("project {} not yet in db ".format(self.name))

            exp_container = None
            barcode_seq = None

            self.datetime_start = datetime.datetime.strptime(
                self.startDatetime_str, "%Y-%m-%dT%H:%M:%S")

            try:
                curr_project = LP_Project.objects.create(namespace=la_namespace,
                                                         name=self.name,
                                                         name_full=".".join(
                                                             (la_namespace.name, self.name)),
                                                         title=self.title, description=self.description,
                                                         parent=parent_project,
                                                         datetime_added=self.datetime_start,
                                                         datetime_start=self.datetime_start,
                                                         remarks=self.remarks)

                for curr_user in self.users:
                    logging.debug("curr- user: {}".format(curr_user))
                    curr_user = Entity.objects.get(acronym=curr_user)

                    if curr_user is not None:
                        self.users_lst.append(curr_user)
                        curr_experiment.users.add(curr_user)

                logging.debug("generated: {}".format(self.name_full))
            except Exception as err:
                sys.stderr.write(f"ERROR adding project {err} ")

        else:
            logging.debug(
                f"root and project item exist, skipping this entry [{self.name}]")


class Experiment:
    """ Class doc """
    """ Class doc """
    __instances = set()
    __proj_leafes = set()
    __instance_list = []

    container_non_existing = []
    exp_non_parents = []

    # s. http://effbot.org/pyfaq/how-do-i-get-a-list-of-all-instances-of-a-given-class.htm
    @classmethod
    def addProjLeaf(proj, parent):
        if parent in __proj_leafes:
            __proj_leafes -= parent
        else:
            _proj_leafes.add(proj)

    @classmethod
    def getProjLeafes(cls):
        for leaf in __proj_leafes:
            yield leaf

    @classmethod
    def getInstancesFromList(cls):
        for ref in cls.__instance_list:
            yield ref()

    @classmethod
    def getInstances(cls):
        dead = set()
        for ref in cls.__instances:
            obj = ref()
            if obj is not None:
                yield obj
            else:
                dead.add(ref)
        cls.__instances -= dead

    def __init__(self, parent_project=None,
                 parent_project_name="",
                 parent_experiment_name="",
                 name="",
                 title="",
                 description="",
                 users=["LAR"],
                 startDatetime="1969-06-09 12:21",
                 remarks="",
                 parameters=None,
                 devices=[],
                 containers=[],
                 wells=[],
                 evaluation=None):

        self.parent_project = parent_project
        self.parent_project_name = parent_project_name
        self.parent_experiment_name = parent_experiment_name
        self.name = name
        self.title = title
        self.description = description
        self.parameters = parameters
        self.users = users
        self.startDatetime_str = startDatetime
        self.remarks = remarks
        self.devices_names = devices
        self.container_names = containers
        self.wells = wells
        self.evaluation = evaluation

        self.users_lst = []

        self.__instances.add(weakref.ref(self))
        self.__instance_list.append(weakref.ref(self))

    def add2DB(self, namespace):
        logging.debug(
            ">>~~~~~~~~~~ add exp db implementation {}".format(self.name))

        self.name_full = ".".join((namespace, self.name))
        parent_experiment = None

        # experiments need a parent project
        if self.parent_project_name != "":
            self.parent_project_name_full = ".".join(
                (namespace, self.parent_project_name))
            parent_project = LP_Project.objects.get(
                name_full=self.parent_project_name_full)
        else:
            self.parent_project_name_full = ".".join(
                (namespace, self.parent_project.name))
            logging.debug(
                "Exp-  parent proj name: {}".format(self.parent_project_name_full))
            parent_project = LP_Project.objects.get(
                name_full=self.parent_project_name_full)

        if parent_project is None:
            sys.stderr.write(
                f"ERROR: The desired parent ({self.parent_project_name_full})  of {self.name} does not exist !!!\n")
            Project.proj_non_parents.append(self.parent_project_name_full)
            return

        logging.debug("Exp. parent proj name: {}".format(
            self.parent_project_name_full))

        # handling experiment names

        # check, if parent exists
        if self.parent_experiment_name != "":
            self.parent_experiment_name_full = ".".join(
                (namespace, self.parent_experiment_name))
            parent_experiment = LP_Experiment.objects.get(
                name_full=self.parent_experiment_name_full)

        if parent_experiment is None:
            sys.stderr.write(
                f"WARNING: No parent of {self.name} specified !!!\n")

        logging.debug(" namespace handling {}".format(1))

        # check namespace
        la_namespace = Namespace.objects.get(namespace=namespace)

        if la_namespace is None:
            sys.stderr.write(
                f"ERROR: Namespace [{namespace}] does not exist in this Database !!!\n")
            # collect wrong namespaces ?
            return

        logging.debug(" device handling {}".format(1))

        device_list = []

        # skipping the loop for now - should be later implemented
        for device_name in []:  # self.devices_names:

            device = Device.objects.get(name=device_name)

            if device is not None:
                device_list.append(device)

                # else: "default_device" ??

            if self.parameters is not None:
                #it_parameters = ";".join([ exp_param for exp_param in project_dict["parameters"] ])
                pass

            # handling of sample source
            # ~ if 'sampleSource' in project_dict:
                # ~ it_sample_source = ";".join([ sa_src for sa_src in project_dict["sampleSource"] ])
            # ~ else :
                # ~ it_sample_source = ""

            # handling of reference Experiment
            # ~ if 'referenceExperiment' in project_dict:
                # ~ ## change to nicer try; except: ...
                # ~ if project_dict['referenceExperiment'] != "":
                # ~ it_reference_experiment = Project.objects.get(name=project_dict['referenceExperiment'])
                # ~ else :
                # ~ it_reference_experiment = Project.objects.get(name='root')
            # ~ else :
                # ~ it_reference_experiment = Project.objects.get(name='root')

        logging.debug("container handling {}".format(1))
        for container_name in self.container_names:
            if Container.objects.filter(name=container_name).exists():
                # creating experiment for each sequence
                pass
            else:
                Experiment.container_non_existing.append(container_name)
                logging.debug(
                    "ERROR: container is not in db: {} !".format(container_name))
                return

        # experiment handling
        # check, if experiment exists
        if not LP_Experiment.objects.filter(name=self.name).exists():
            # parent_it = Project.objects.get(name=)
            logging.debug("experiment {} not yet in db ".format(self.name))

            exp_container = None
            barcode_seq = None

            self.datetime_start = datetime.datetime.strptime(
                self.startDatetime_str, "%Y-%m-%dT%H:%M:%S")

            try:
                curr_experiment = LP_Experiment.objects.create(namespace=la_namespace,
                                                               name=self.name,
                                                               name_full=".".join(
                                                                   (la_namespace.name, self.name)),
                                                               title=self.title, description=self.description,
                                                               datetime_added=self.datetime_start,
                                                               datetime_start=self.datetime_start,
                                                               remarks=self.remarks)
                for curr_user in self.users:
                    logging.debug("curr- user: {}".format(curr_user))
                    curr_user = Entity.objects.get(acronym=curr_user)

                    if curr_user is not None:
                        self.users_lst.append(curr_user)
                        curr_experiment.users.add(curr_user)

                if parent_experiment is not None:
                    curr_experiment.parent = parent_experiment
                    curr_experiment.save()

                parent_project.experiments.add(curr_experiment)

                logging.debug("generated: {}".format(self.name_full))
            except Exception as err:
                sys.stderr.write(f"ERROR adding experiment {err} ")

        else:
            logging.debug(
                f"root and experiment item exist, skipping this entry [{self.name}]")


class AddPythonProjects2DB():
    def __init__(self, filename=None, namespace="de.unigreifswald.biochem.akb",
                 prettyPrint=False, replace_data=False):

        self.namespace = namespace
        la_namespace = Namespace.objects.get(namespace=namespace)

        if la_namespace is None:
            sys.stderr.write(
                f"ERROR: Namespace [{namespace}] does not exist in this Database !!!\n")
            return

        self.exp_dict = {}  # dictionary for experiments
        self.eval_dict = {}  # dictionary for evaluations
        self.replace_data = replace_data

        if self.replace_data:
            sys.stderr.write("WARNING: Replacing data in db !!!")

        # ensure that database has a root item
        root_name = "root"
        try:
            self.proj_root = LP_Project.objects.get(name=root_name)
        except ObjectDoesNotExist:
            self.proj_root = LP_Project.objects.create(namespace=la_namespace,
                                                       name=root_name,
                                                       name_full=".".join(
                                                           (la_namespace.name, root_name)),
                                                       title="root item of project item tree",
                                                       description="root item of project item tree",
                                                       datetime_start=datetime.datetime.now(
                                                           datetime.timezone.utc),
                                                       remarks="root needs to be present")

        with open(filename, 'r') as proj_file:
            # safe_exec(proj_file.read())

            exec(proj_file.read())

        try:
            for obj in Project.getInstancesFromList():
                obj.add2DB(self.namespace)
        except Exception as err:
            sys.stderr.write("ERROR-Adding Projects: {}\n".format(err))

        try:
            for obj in Experiment.getInstancesFromList():
                print(obj.name)
                obj.add2DB(self.namespace)
        except Exception as err:
            sys.stderr.write("ERROR-Adding Experiments: {}\n".format(err))

        if prettyPrint:
            self.prettyPrint()
        else:
            logging.debug(
                f"The follwing Projects had no parents in db:\n  {','.join(Project.proj_non_parents) }\n")
            logging.debug(
                f"The follwing Containers have not been found in Containers db:\n  {','.join(Experiment.container_non_existing) }\n")
            logging.debug(
                "1.2a >>>>>>>>> adding proj {} to db".format(filename))
            #~ self.addProjectDB(project_dict=self.json_dict)

    def prettyPrint(self):
        """Python pretty printer """
        #~ print( json.dumps(self.json_dict, indent=2, separators=(',', ':'), sort_keys=True))

        logging.debug(
            "Python pretty printer ... not implemented yet {}".format())
