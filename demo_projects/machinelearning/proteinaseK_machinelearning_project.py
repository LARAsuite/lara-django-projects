
# root project

namespace = "de.unigreifswald.biochem.akb.lara.machinelearning"

name = "ProtKml"

title = "ProteinaseK Machine Learning Demo Project"

description = """Demonstration of a Machine Learning Project in LARA: The activity of the enzyme **ProteinaseK** shall be increased
with the help of a Machine Learning approach. The Machine Learning Model will be a simple Linear Model."""

users = ["SBO","MDO"]

startDatetime = "2019-07-21T00:00:00"

remarks = "remarks"
      
protK = Project(parent_project="root", namespace=namespace, name=name, title=title, description=description, users=users, 
                   startDatetime=startDatetime, remarks=remarks)


# subprojects

name = "ProtKml.ProposeStartingSequencesPro"

title = "Proposal of good Starting Sequences"

description = """Determination of reasonable ProteinaseK starting sequences"""

remarks = "remarks"

startDatetime = "2019-07-21T00:00:00"
    
protK_start = Project(protK, namespace=namespace, name=name, title=title, description=description, users=users, 
                 startDatetime=startDatetime, remarks=remarks)


name = "ProtKml.StartSequenceSearchExp"

title = "Searching suitable Sequences"

description = """Searching suitable Sequences"""

remarks = "remarks"

protK_start_seq = Experiment(protK_start, namespace=namespace, name=name, title=title, description=description, users=users, 
                             startDatetime=startDatetime, remarks=remarks)


# Rich Regression
name = "ProtKml.RichRegressionPro"

title = "Suggesting new Variants based on Rich Regression ML approach"

description = """Rich Regression shall be applied ...."""

remarks = "remarks"

startDatetime = "2019-07-22T00:00:00"
    
protK_rrp = Project(protK, namespace=namespace, name=name, title=title, description=description, users=users, 
                 startDatetime=startDatetime, remarks=remarks)


name = "ProtKml.RichRegressionExp"

title = "Rich Regression Learning"

description = """Rich Regression Experiment"""

remarks = "remarks"

protK_rrexp1 = Experiment(protK_rrp, namespace=namespace, name=name, title=title, description=description, users=users, 
                             startDatetime=startDatetime, remarks=remarks)

# LOSS
name = "ProtKml.LOSSPro"

title = "Suggesting new Variants based on LOSS ML approach"

description = """LOSS shall be applied ...."""

remarks = "remarks"

startDatetime = "2019-07-23T00:00:00"
    
protK_lossp = Project(protK, namespace=namespace, name=name, title=title, description=description, users=users, 
                 startDatetime=startDatetime, remarks=remarks)


name = "ProtKml.LOSSExp"

title = "LOSS Learning"

description = """LOSS Experiment"""

remarks = "remarks"

protK_loss1_exp = Experiment(protK_lossp, namespace=namespace, name=name, title=title, description=description, users=users, 
                             startDatetime=startDatetime, remarks=remarks)
