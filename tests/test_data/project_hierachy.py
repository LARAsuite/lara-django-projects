print("External: project test hierachy")

p1 = Project(None, name="proj p1")

p2 = Project(p1, name="proj p2")

p3 = Project(p2, name="proj p3")

logging.debug("\n\n------ printing {} --------\n".format("Names") )

p3.printNamesRec()


logging.debug("\n\n------ evaluating {} --------\n".format("Bottom - > Top ") )
#~ p3.evalBottomTop()
logging.debug("\n\n------ evaluating {} --------\n".format("Top -> Bottom") )
#~ p3.evalTopBottom()
#~ logging.debug("ext cl_ob 1 para is: {}".format(cl_obj1.parameters) )
