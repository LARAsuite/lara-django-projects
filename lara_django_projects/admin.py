"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_projects admin *

:details: lara_django_projects admin module admin backend configuration.
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - run "lara-django-dev admin_generator lara_django_projects >> admin.py" to update this file
________________________________________________________________________
"""
# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import Experiment, Project, ExperimentSelection, ProjSelection, ProjectsCalendar, ExperimentCalendar, OutputTemplate, Report, ProjectSynchronisationSettings


@admin.register(Experiment)
class ExperimentAdmin(admin.ModelAdmin):
    list_display = (
        'name_full',
       
        'namespace',
        'name',
        
        'title',
        'parent',
      
        'datetime_start',
        'datetime_added',
        'status',
     
        'outcome',
     
        'reference_sample_source',
        'parameters',
        'reference_experiment',
     
        'description',
        'remarks',
       
    )
    list_filter = (
        'namespace',
        'parent',
        'experiment_design',
        'datetime_start',
        'datetime_added',
        'status',
        'outcome',
        'view_selected',
        'view_collapsed',
        'reference_experiment',
    )
    raw_id_fields = ('users', 'data', 'evaluation', 'method', 'procedure', 'process', 'substances', 'polymers', 'mixtures', 'parts', 'devices', 'labwares', 'organisms', 'samples', 'tags', )
    search_fields = ('name',)


@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    list_display = (
        'name_full',
        'namespace',
        'name',
        
        'title',
        'parent',
        
        'datetime_start',
        'datetime_added',
        'status',
        'status_outcome',
      
        'description',
        'remarks',
      
    )
    list_filter = (
        'namespace',
        'parent',
        'datetime_start',
        'datetime_added',
        'status',
        'status_outcome',
        'view_selected',
        'view_collapsed',
    )
    raw_id_fields = ('users', 'experiments', 'data', 'evaluation', 'tags')
    search_fields = ('name',)


@admin.register(ExperimentSelection)
class ExperimentSelectionAdmin(admin.ModelAdmin):
    list_display = ('experiment_sel_id', 'name', 'user')
    list_filter = ('user',)
    raw_id_fields = ('selected_items',)
    search_fields = ('name',)


@admin.register(ProjSelection)
class ProjSelectionAdmin(admin.ModelAdmin):
    list_display = ('project_sel_id', 'name', 'user')
    list_filter = ('user',)
    raw_id_fields = ('selected_items',)
    search_fields = ('name',)


@admin.register(ProjectsCalendar)
class ProjectsCalendarAdmin(admin.ModelAdmin):
    list_display = (
        'title',
        'calendar_URL',
        'summary',
        'description',
        'color',
        'ics',
        'start_datetime',
        'end_datetime',
        'duration',
        'all_day',
        'created',
        'last_modified',
        'timestamp',
        'location',
        'geolocation',
        'conference_URL',
        'range',
        'related',
        'role',
        'tzid',
        'offset_UTC',
        'alarm_repeat_JSON',
        'event_repeat',
        'event_repeat_JSON',
    
        'user',
        'project',
    )
    list_filter = (
        'start_datetime',
        'end_datetime',
        'all_day',
        'created',
        'last_modified',
        'timestamp',
        'location',
        'geolocation',
        'user',
        'project',
    )
    raw_id_fields = ('tags', 'attendees')


@admin.register(ExperimentCalendar)
class ExperimentCalendarAdmin(admin.ModelAdmin):
    list_display = (
        'title',
        'calendar_URL',
        'summary',
        'description',
        'color',
        'ics',
        'start_datetime',
        'end_datetime',
        'duration',
        'all_day',
        'created',
        'last_modified',
        'timestamp',
        'location',
        'geolocation',
        'conference_URL',
        'range',
        'related',
        'role',
        'tzid',
        'offset_UTC',
        'alarm_repeat_JSON',
        'event_repeat',
        'event_repeat_JSON',
        'calendar_id',
        'user',
        'experiment',
    )
    list_filter = (
        'start_datetime',
        'end_datetime',
        'all_day',
        'created',
        'last_modified',
        'timestamp',
        'location',
        'geolocation',
        'user',
        'experiment',
    )
    raw_id_fields = ('tags', 'attendees')


@admin.register(OutputTemplate)
class OutputTemplateAdmin(admin.ModelAdmin):
    list_display = (
        'template_id',
        'namespace',
        'name',
        'description',
        'format',
        'template_text',
        'file',
        'datetime_created',
        'datetime_updated',
        'version',
    )
    list_filter = ('namespace', 'datetime_created', 'datetime_updated')
    search_fields = ('name',)


@admin.register(Report)
class ReportAdmin(admin.ModelAdmin):
    list_display = (
        'report_id',
        'name',
        'IRI',
        'handle',
        'description',
        'template',
        'format',
        'text',
        'file',
        'PDF',
        'datetime_created',
        'datetime_updated',
    )
    list_filter = ('template', 'datetime_created', 'datetime_updated')
    raw_id_fields = ('authors',)
    search_fields = ('name',)


@admin.register(ProjectSynchronisationSettings)
class ProjectSynchronisationSettingsAdmin(admin.ModelAdmin):
    list_display = (
        'sync_id',
        'name',
        'target_server',
        'login',
        'pass_wd',
        'auth_key',
        'cert',
        'datetime_started',
        'duration',
        'event_repeat',
        'event_repeat_JSON',
        'logs',
        'description',
    )
    list_filter = ('datetime_started',)
    raw_id_fields = ('projects', 'experiment')
    search_fields = ('name',)
