
# root project

name = "LARAvalidation"

title = "LARA Robot Validation"

description = """LARA Robot validation experiments to evaluate, document and monitor the performance 
of the Greifswald robotic high-troughput protein engineering platform LARA """

users = ["AWE","MDO"]

startDatetime = "2018-12-07T00:00:00"

remarks = "remarks"
      
lara_val = Project(parent_project_name="root", name=name,
                   title=title, description=description, users=users, startDatetime=startDatetime, remarks=remarks)

# subprojects

name = "LiquidHandlerValidation"

title = "Liquid Handler Validation"

description = """Determination of a feasible system for liquid handler validation"""

users = ["AWE","MDO"]


remarks = "remarks"
    
lh_val = Project(lara_val, name=name, title=title, description=description, users=users, 
                 startDatetime=startDatetime, remarks=remarks)


name = "EvaporationControl"

title = "Evaporation Control"

description = """Determination of evaporation rates for different volumes of water and 1mM and 10 mM NaCl solutions"""

users = ["AWE","MDO"]


remarks = "remarks"

lh_evap_crtl= Project(lh_val, name=name, title=title, description=description, users=users, 
                      startDatetime=startDatetime, remarks=remarks)


name = "GravimetricValidation"

title = "Gravimetric Validation"

description = """Determination of pipetting accuracy of pipetting robot in lower/mid/high range through measurement of liquid weight in well columns and rows"""

users = ["AWE","MDO"]

startDatetime = "2018-12-12T00:00:00"

remarks = "remarks"

lh_grav_val = Project(lh_val, name=name, title=title, description=description, users=users, 
                      startDatetime=startDatetime, remarks=remarks)


name = "201812122ulInEmptyCol"

title = "2uL In Empty Column Plate Well"

description = """Pipetting of 2ul water into each well of a 96 well plate with removable column strips and measurement of weight difference between empty and filled column strips"""

users = ["AWE","MDO"]

remarks = "remarks"

lh_grav_2ulcol = Experiment(lh_grav_val, name=name, title=title, description=description, users=users, 
                            startDatetime=startDatetime, remarks=remarks)


name = "201812122ulInEmptyColRev"

title = "2uL In Empty Column Plate Well Reverse"

description = """Pipetting of 2ul water into each well of a 96 well plate with removable column strips in reversed alignment and measurement of weight difference between empty and filled column strips"""

users = ["AWE","MDO"]

remarks = "remarks"

lh_grav_2ulcolrev = Experiment(lh_grav_val, name=name, title=title, description=description, users=users, 
                               startDatetime=startDatetime, remarks=remarks)


name = "2018121210ulInEmptyCol"

title = "10uL In Empty Column Plate Well"

description = """Pipetting of 10ul water into each well of a 96 well plate with removable column strips and measurement of weight difference between empty and filled column strips"""

users = ["AWE","MDO"]

remarks = "remarks"

lh_grav_10ulcol = Experiment(lh_grav_val, name=name, title=title, description=description, users=users, 
                             startDatetime=startDatetime, remarks=remarks)


name = "2018121210ulInEmptyColRev"

title = "10uL In Empty Column Plate Well Reverse"

description = """Pipetting of 10ul water into each well of a 96 well plate with removable column strips in reversed alignment and measurement of weight difference between empty and filled column strips"""

users = ["AWE","MDO"]

remarks = "remarks"

lh_grav_10ulcolrev = Experiment(lh_grav_val, name=name, title=title, description=description, users=users, 
                                startDatetime=startDatetime, remarks=remarks)


name = "2018121210ulInEmptyRow"

title = "10uL In Empty Row Plate Well"

description = """Pipetting of 10ul water into each well of a 96 well plate with removable row strips and measurement of weight difference between empty and filled row strips"""

users = ["AWE","MDO"]

remarks = "remarks"

lh_grav_10ulrow = Experiment(lh_grav_val, name=name, title=title, description=description, users=users, 
                             startDatetime=startDatetime, remarks=remarks)


name = "2018121210ulInEmptyColTipTouch"

title = "10uL In Empty Column Plate Well Tip Touch"

description = """Pipetting of 10ul water into each well of a 96 well plate with removable column strips with tip touch to well wall and measurement of weight difference between empty and filled column strips"""

users = ["AWE","MDO"]

remarks = "remarks"

lh_grav_10ulcoltt = Experiment(lh_grav_val, name=name, title=title, description=description, users=users, 
                               startDatetime=startDatetime, remarks=remarks)


name = "2018121210ulIn100ulCol"

title = "10uL In 100ul Column Plate Well"

description = """Pipetting of 10ul water into each well of a 96 well plate (prefilled with 100 ul water) with removable column strips and measurement of weight difference between pre-filled and filled column strips"""

users = ["AWE","MDO"]

remarks = "remarks"

lh_grav_10uladdcol = Experiment(lh_grav_val, name=name, title=title, description=description, users=users, 
                                startDatetime=startDatetime, remarks=remarks)


name = "20181212100ulIn100ulCol"

title = "100uL In 100ul Column Plate Well"

description = """Pipetting of 100ul water into each well of a 96 well plate (prefilled with 100 ul water) with removable column strips and measurement of weight difference between pre-filled and filled column strips"""

users = ["AWE","MDO"]

remarks = "remarks"

lh_grav_100uladdcol = Experiment(lh_grav_val, name=name, title=title, description=description, users=users, 
                                 startDatetime=startDatetime, remarks=remarks)


name = "OneStepPipetting"

title = "One Step Pipetting"

description = """Simult. precision and accuracy measurements utilizing a dual dye system"""

users = ["AWE","MDO"]

startDatetime = "2018-12-14T00:00:00"

remarks = "remarks"

lh_onestep_val = Project(lh_val, name=name, title=title, description=description, users=users, 
                         startDatetime=startDatetime, remarks=remarks)


name = "DualDyeCoClNiSO"

title = "Dye Ratio CoCl2 NiSO4"

description = """Dual dye measurements with CoCl2 and NiSO4 dye pair"""

users = ["AWE","MDO"]

remarks = "remarks"

dd_cocl_niso = Project(lh_onestep_val, name=name, title=title, description=description, users=users, startDatetime=startDatetime, remarks=remarks)


name = "20181214DualDyeCoClNiSODilutionSeries"

title = "Dye Ratio CoCl2 NiSO4 Dilution Series"

description = """Dilution series with CoCl2 and NiSO4 dye pair at final volumes of 200, 150 and 100 uL"""

users = ["AWE","MDO"]

remarks = "remarks"

containers = ["VA18121402"]

ds_cocl_niso = Experiment(dd_cocl_niso, name=name, title=title, description=description, users=users, 
                          startDatetime=startDatetime, remarks=remarks, containers=containers)


name = "20190109DualDyeCoClNiSODilutionFromStock"

title = "Dye Ratio CoCl2 NiSO4 Dilution Series From Stock Solution"

description = """Dilution series with CoCl2 and NiSO4 dye pair at final volumes of 200 and 100 uL from a NiSO4 stock solution"""

users = ["AWE","MDO"]

startDatetime = "2019-01-09T00:00:00"

remarks = "remarks"

containers = ["VA19010901"]

ds_cocl_niso = Experiment(dd_cocl_niso, name=name, title=title, description=description, users=users, startDatetime=startDatetime, remarks=remarks, containers=containers)


name = "20190109BravoCoClNiSO"

title = "Bravo CoCl2 NiSO4"

description = """Dual dye measurements with CoCl2 and NiSO4 dye pair pipetted by Bravo liquid handler to determine accuracy and precision at pipetting volumes of 10, 20 and 50 uL"""

users = ["AWE","MDO"]

remarks = "remarks"

containers= ["VA19010902","VA19010903","VA19010904"]

bravo_cocl_niso = Experiment(dd_cocl_niso, name=name, title=title, description=description, users=users, 
                             startDatetime=startDatetime, remarks=remarks, containers=containers)


name = "DualDyeRhodBNiSO"

title = "Dye Ratio Rhodamin B NiSO4"

description = """Dual dye measurements with Rhodamin B and NiSO4 dye pair"""

users = ["AWE","MDO"]

startDatetime = "2018-12-19T00:00:00"

remarks = "remarks"

dd_rhodb_niso = Project(lh_onestep_val, name=name, title=title, description=description, users=users, 
                        startDatetime=startDatetime, remarks=remarks)


name = "20181218DualDyeRhodBNiSODilutionSeries"

title = "Dye Ratio RhodB NiSO4 Dilution Series"

description = """Dilution series with Rhodamin B and NiSO4 dye pair at final volumes of 200, 150 and 100 uL"""

users = ["AWE","MDO"]

startDatetime = "2018-12-18T00:00:00"

remarks = "remarks"

containers = ["VA18121801"]

ds_rhodb_niso = Experiment(dd_rhodb_niso, name=name, title=title, description=description, users=users, 
                startDatetime=startDatetime, remarks=remarks, containers=containers)


name = "DualDyeRhodBOrangeG"

title = "Dye Ratio Rhodamin B Orange G"

description = """Dual dye measurements with Rhodamin B and Orange G dye pair"""

users = ["AWE","MDO"]

startDatetime = "2019-01-17T00:00:00"

remarks = "remarks"

dd_rhodb_orangeg = Project(lh_onestep_val, name=name, title=title, description=description, users=users, 
                   startDatetime=startDatetime, remarks=remarks)


name = "20190129DualDyeRhodBOrangeGDilutionSeries"

title = "Dye Ratio RhodB OrangeG Dilution Series"

description = """Dilution series with Rhodamin B and OrangeG dye pair at final volumes of 200 and 100 uL"""

users = ["AWE","MDO"]

startDatetime = "2019-01-29T00:00:00"

remarks = "remarks"

containers = ["VA19012901"]

ds_rhodb_og = Experiment(dd_rhodb_orangeg, name=name, title=title, description=description, 
               users=users, startDatetime=startDatetime, remarks=remarks, containers=containers)


name = "20190117BravoRhodBOrangeG"

title = "Bravo Rhodamin B Orange G"

description = """Dual dye measurements with Rhodamin B and Orange G dye pair pipetted by Bravo liquid handler to determine accuracy and precision at pipetting volumes of 10, 20 and 50 uL"""

users = ["AWE","MDO"]

startDatetime = "2019-01-17T00:00:00"

remarks = "remarks"

containers = ["VA19011701","VA19011702","VA19011703","VA19011704"]

bravo_rhodb_orangeg = Experiment(dd_rhodb_orangeg , name=name, title=title, description=description, users=users, 
                        startDatetime=startDatetime, remarks=remarks, containers=containers)


name = "DyeSpectra"

title = "Dye Spectra"

description = """Finding best dye pairs for dual dye experiments"""

users = ["AWE","MDO"]

startDatetime = "2018-12-14T00:00:00"

remarks = "remarks"

dye_spec = Project(lara_val, name=name, title=title, description=description, users=users, 
                    startDatetime=startDatetime, remarks=remarks)


name = "20181214SpectrumCoCl2"

title = "Spectrum CoCl2"

description = """Spectrum of CoCl2 solutions at concentrations of 500, 50 and 5 mM"""

users = ["AWE","MDO"]


remarks = "remarks"

containers = ["VA18121401"]

wells = ["A1-A3"]

spec_cocl = Experiment(dye_spec, name=name, title=title, description=description, users=users, 
                         startDatetime=startDatetime, remarks=remarks, containers=containers, wells=wells)


name = "20181214SpectrumNiNO"

title = "Spectrum NiNO"

description = """Spectrum of NiNO solutions at concentrations of 500, 50 and 5 mM"""

users = ["AWE","MDO"]

remarks = "remarks"

containers = ["VA18121401"]

wells = ["B1-B3"]

spec_nino = Experiment(dye_spec, name=name, title=title, description=description, users=users, 
                        startDatetime=startDatetime, remarks=remarks, containers=containers, wells=wells)

name = "20181214SpectrumRhodB"

title = "Spectrum Rhodamin B"

description = """Spectrum of Rhodamin B solutions at concentrations of 50, 5, 0.5 and 0.05 mM"""

users = ["AWE","MDO"]

remarks = "remarks"

containers = ["VA18121401"]

wells = ["C1-C4"]

spec_rhodb = Experiment(dye_spec, name=name, title=title, description=description, users=users, 
                        startDatetime=startDatetime, remarks=remarks, containers=containers, wells=wells)


name = "20181214SpectrumTolB"

title = "Spectrum Toluidin Blue"

description = """Spectrum of Toluidin Blue solutions at concentrations of 50, 5, 0.5 and 0.05 mM"""

users = ["AWE","MDO"]

remarks = "remarks"

containers = ["VA18121401"]

wells = ["D1-D4"]

spec_tolb = Experiment(dye_spec, name=name, title=title, description=description, users=users, 
                        startDatetime=startDatetime, remarks=remarks, containers=containers, wells=wells)


name = "20181214SpectrumCoNO"

title = "Spectrum CoNO"

description = """Spectrum of CoNO solutions at concentrations of 500, 50 and 5 mM"""

users = ["AWE","MDO"]

remarks = "remarks"

containers = ["VA18121401"]

wells = ["F1-F3"]

spec_cono = Experiment(dye_spec, name=name, title=title, description=description, users=users, 
                         startDatetime=startDatetime, remarks=remarks, containers=containers, wells=wells)


name = "20181214SpectrumNiSO"

title = "Spectrum NiSO"

description = """Spectrum of NiSO solutions at concentrations of 500, 50 and 5 mM"""

users = ["AWE","MDO"]

remarks = "remarks"

containers = ["VA18121401"]

wells = ["E1-E3"]

spec_niso = Experiment(dye_spec, name=name, title=title, description=description, users=users, 
                        startDatetime=startDatetime, remarks=remarks, containers=containers, wells=wells)


name = "20181218SpectrumHolmium"

title = "Spectrum Holmium Perchlorate"

description = """Spectrum of a 13 ww Holmium Perchlorate solution"""

users = ["AWE","MDO"]

startDatetime = "2018-12-18T00:00:00"

remarks = "remarks"

containers = ["VA18121802"]

spec_holmium = Experiment(dye_spec, name=name, title=title, description=description, users=users, startDatetime=startDatetime, remarks=remarks, containers=containers)


name = "20190117SpectrumOrangeG"

title = "Spectrum OrangeG"

description = """Spectrum of a 95 uM Orange G solution"""

users = ["AWE","MDO"]

startDatetime = "2019-01-17T00:00:00"

remarks = "remarks"

containers = ["VA19011602"]

wells = ["C5"]

spec_orangeg = Experiment(dye_spec, name=name, title=title, description=description, users=users, 
                startDatetime=startDatetime, remarks=remarks, containers=containers, wells=wells)


name = "SpectrometerValidation"

title = "Spectrometer Validation"

description = """Determination of a feasible system for spectrometer validation"""

users = ["AWE","MDO"]

startDatetime = "2018-12-07T00:00:00"

remarks = "remarks"
    
sr_val = Project(lara_val, name=name, title=title, description=description, users=users, 
                   startDatetime=startDatetime, remarks=remarks)


name = "ValidationVarioskanLUX"

title = "Validation VarioskanLUX"

description = """Validation of spectrometer VarioskanLUX"""

users = ["AWE","MDO"]

startDatetime = "2019-01-16T00:00:00"

remarks = "remarks"
    
sr_val_vsl = Project(sr_val, name=name, title=title, description=description, users=users, 
                         startDatetime=startDatetime, remarks=remarks)


name = "20190116VarioskanLUXOrangeGDilution"

title = "VarioskanLUX Orange G Dilution Series"

description = """Validation of spectrometer VarioskanLUX using OrangeG dilution series following a BMG Labtech calibration protocol"""

users = ["AWE","MDO"]

remarks = "remarks"

containers = ["VA19011601"]
    
vsl_og = Experiment(sr_val_vsl, name=name, title=title, description=description, users=users, 
                     startDatetime=startDatetime, remarks=remarks, containers=containers)


name = "ValidationVarioskan"

title = "Validation Varioskan"

description = """Validation of spectrometer Varioskan"""

users = ["AWE","MDO"]

remarks = "remarks"
    
sr_val_vs = Project(sr_val, name=name, title=title, description=description, users=users, 
                     startDatetime=startDatetime, remarks=remarks)


name = "20190116VarioskanOrangeGDilution"

title = "Varioskan Orange G Dilution Series"

description = """Validation of spectrometer Varioskan using OrangeG dilution series following a BMG Labtech calibration protocol"""

users = ["AWE","MDO"]

remarks = "remarks"

containers = []
    
vs_og = Experiment(sr_val_vs, name=name, title=title, description=description, users=users, 
                      startDatetime=startDatetime, remarks=remarks, containers=containers)


name = "ValidationFLUOstarOmega"

title = "Validation FLUOstar Omega"

description = """Validation of spectrometer FLUOstar Omega"""

users = ["AWE","MDO"]

remarks = "remarks"
    
sr_val_fso = Project(sr_val, name=name, title=title, description=description, users=users, 
                      startDatetime=startDatetime, remarks=remarks)


name = "20190116FLUOstarOmegaOrangeGDilution"

title = "FLUOstar Omega Orange G Dilution Series"

description = """Validation of spectrometer FLUOstar Omega using OrangeG dilution series following a BMG Labtech calibration protocol"""

users = ["AWE","MDO"]

remarks = "remarks"

containers = []
    
fso_og = Experiment(sr_val_fso, name=name, title=title, description=description, users=users, 
                     startDatetime=startDatetime, remarks=remarks, containers=containers)


name = "MultidropValidation"

title = "Multidrop Validation"

description = """Validation of pipetting precision and accuracy of Multidrop Combi tip cassettes"""

users = ["AWE","MDO"]

startDatetime = "2019-02-07T00:00:00"

remarks = "remarks"
    
multidrop_val = Project(lara_val, name=name, title=title, description=description, users=users, 
                   startDatetime=startDatetime, remarks=remarks)


name = "GravimetricValidation"

title = "Gravimetric Validation"

description = """Determination of pipetting accuracy of tip cassettes"""

users = ["AWE","MDO"]

remarks = "remarks"
    
md_grav_val = Project(multidrop_val, name=name, title=title, description=description, users=users, 
                       startDatetime=startDatetime, remarks=remarks)


name = "20190207100ulTip1"

title = "100ul Tip1"

description = """Gravimetric validation of pipetting accuracy of each tip of Multidrop Standard Tip Cassette Tip1 for a volume of 100 ul"""

users = ["AWE","MDO"]

remarks = "remarks"

containers = []
    
md_grav_tip1_100ul = Experiment(md_grav_val, name=name, title=title, description=description, users=users, 
                                startDatetime=startDatetime, remarks=remarks, containers=containers)


name = "2019020710ulTip1"

title = "10ul Tip1"

description = """Gravimetric validation of pipetting accuracy of each tip of Multidrop Standard Tip Cassette Tip1 for a volume of 10 ul"""

users = ["AWE","MDO"]

remarks = "remarks"

containers = []
    
md_grav_tip1_10ul = Experiment(md_grav_val, name=name, title=title, description=description, users=users, 
                               startDatetime=startDatetime, remarks=remarks, containers=containers)


name = "2019020710ulStip1"

title = "10ul STip1"

description = """Gravimetric validation of pipetting accuracy of each tip of Multidrop Small Tip Cassette STip1 for a volume of 10 ul"""

users = ["AWE","MDO"]

remarks = "remarks"

containers = []
    
md_grav_stip1_10ul = Experiment(md_grav_val, name=name, title=title, description=description, users=users,
                                startDatetime=startDatetime, remarks=remarks, containers=containers)


name = "201902075ulSTip1"

title = "5ul STip1"

description = """Gravimetric validation of pipetting accuracy of each tip of Multidrop Small Tip Cassette STip1 for a volume of 2 x 5 ul"""

users = ["AWE","MDO"]

remarks = "remarks"

containers = []
    
md_grav_stip1_5ul = Experiment(md_grav_val, name=name, title=title, description=description, users=users,
                               startDatetime=startDatetime, remarks=remarks, containers=containers)


name = "2019020710ulStip2"

title = "10ul STip2"

description = """Gravimetric validation of pipetting accuracy of each tip of Multidrop Small Tip Cassette STip2 for a volume of 10 ul"""

users = ["AWE","MDO"]

remarks = "remarks"

containers = []
    
md_grav_stip2_10ul = Experiment(md_grav_val, name=name, title=title, description=description, users=users, 
                                startDatetime=startDatetime, remarks=remarks, containers=containers)


name = "201902075ulSTip2"

title = "5ul STip2"

description = """Gravimetric validation of pipetting accuracy of each tip of Multidrop Small Tip Cassette STip2 for a volume of 2 x 5 ul"""

users = ["AWE","MDO"]

remarks = "remarks"

containers = []
    
md_grav_stip2_5ul = Experiment(md_grav_val, name=name, title=title, description=description, users=users,
                               startDatetime=startDatetime, remarks=remarks, containers=containers)


name = "20190212100ulTip1Calibrated"

title = "100ul Tip1 Calibrated"

description = """Gravimetric validation of pipetting accuracy of each tip of Multidrop Standard Tip Cassette Tip1 for a volume of 100 ul after calibration with standard tube"""

users = ["AWE","MDO"]

startDatetime = "2019-02-12T00:00:00"

remarks = "remarks"

containers = []
    
md_grav_tip1_100ul_cal = Experiment(md_grav_val, name=name, title=title, description=description, users=users, 
                                    startDatetime=startDatetime, remarks=remarks, containers=containers)

name = "OneStepPipetting"

title = "One Step Pipetting"

description = """Determination of pipetting accuracy and precision of tip casettes with NiSO4 and CoCl2 dual dye system"""

users = ["AWE","MDO"]

startDatetime = "2019-02-11T00:00:00"

remarks = "remarks"
    
md_dual_dye = Project(multidrop_val, name=name, title=title, description=description, users=users, 
                      startDatetime=startDatetime, remarks=remarks)


name = "20190211DualDyeTip1"

title = "Dual Dye Tip1"

description = """Determination of accuracy and precision of Multidrop Standard Tip Cassette Tip1 for volumina of 10 and 50 ul via dye ratio of NiSo4 and CoCl2"""

users = ["AWE","MDO"]

remarks = "remarks"
    
containers = []

md_dd_tip1 = Experiment(name=name, title=title, description=description, users=users, 
                        startDatetime=startDatetime, remarks=remarks, containers=containers)


name = "20190211DualDyeSTip1"

title = "Dual Dye STip1"

description = """Determination of accuracy and precision of Multidrop Small Tip Cassette STip1 for a volume of 10 ul via dye ratio of NiSo4 and CoCl2"""

users = ["AWE","MDO"]

remarks = "remarks"
    
md_dd_stip1 = Experiment(md_dual_dye, name=name, title=title, description=description, users=users, 
                         startDatetime=startDatetime, remarks=remarks, containers=containers)


name = "VolumetricValidation"

title = "Volumetric Validation"

description = """Calibration of Multidrop tip cassettes via volumetric standard tube"""

users = ["AWE","MDO"]

remarks = "remarks"
    
md_vol_val = Project(multidrop_val, name=name, title=title, description=description, users=users, 
                      startDatetime=startDatetime, remarks=remarks)


name = "20190211VolumetricTip1"

title = "Volumetric Tip1"

description = """Calibration of Multidrop Standard Tip Cassette Tip1 via volumetric standard tube for a volume of 1 ml"""

users = ["AWE","MDO"]

remarks = "remarks"

containers = []
    
md_vol_tip1 = Experiment(md_vol_val, name=name, title=title, description=description, users=users, 
                          startDatetime=startDatetime, remarks=remarks, containers=containers)


name = "GenesisWorkstationValidation"

title = "Genesis Workstation Validation"

description = """Validation of pipetting precision and accuracy of Tecan Genesis Workstation 150"""

users = ["AWE","MDO"]

startDatetime = "2019-02-12T00:00:00"

remarks = "remarks"
    
genesis_val = Project(lara_val, name=name, title=title, description=description, users=users, 
                       startDatetime=startDatetime, remarks=remarks)


name = "GravimetricValidation"

title = "Gravimetric Validation"

description = """Determination of pipetting accuracy of Genesis Liquid Handler"""

users = ["AWE","MDO"]

remarks = "remarks"
    
gs_grav_val = Project(genesis_val, name=name, title=title, description=description, users=users, 
                       startDatetime=startDatetime, remarks=remarks)


name = "20190213100ul"

title = "100ul Gravimetric Validation"

description = """Gravimetric validation of pipetting accuracy of each tip of Genesis Liquid Handler for a volume of 100 ul"""

users = ["AWE","MDO"]

startDatetime = "2019-02-13T00:00:00"

remarks = "remarks"

containers = []
    
gs_grav_100ul = Experiment(gs_grav_val, name=name, title=title, description=description, users=users, 
                            startDatetime=startDatetime, remarks=remarks, containers=containers)


name = "20190213100ulCalibrated"

title = "100ul Gravimetric Validation Calibrated"

description = """Gravimetric validation of pipetting accuracy of each tip of Genesis Liquid Handler for a volume of 100 ul after adjustment of liquid class calibration factor"""

users = ["AWE","MDO"]

remarks = "remarks"

containers = []
    
gs_grav_100ul_cal = Experiment(gs_grav_val, name=name, title=title, description=description, users=users, 
                      startDatetime=startDatetime, remarks=remarks, containers=containers)


name = "201902142ul"

title = "2ul Gravimetric Validation"

description = """Gravimetric validation of pipetting accuracy of each tip of Genesis Liquid Handler for a volume of 5 x 2 ul in 100 ul"""

users = ["AWE","MDO"]

startDatetime = "2019-02-14T00:00:00"

remarks = "remarks"

containers = []
    
gs_grav_2ul = Experiment(gs_grav_val, name=name, title=title, description=description, users=users, 
                          startDatetime=startDatetime, remarks=remarks, containers=containers)


name = "201902142ulExcess"

title = "2ul Gravimetric Validation Excess"

description = """Gravimetric validation of pipetting accuracy of each tip of Genesis Liquid Handler for a volume of 5 x 2 ul in 100 ul with excess adjusted from 50 ul to 50 percent"""

users = ["AWE","MDO"]

remarks = "remarks"

containers = []
    
gs_grav_2ul_xs = Experiment(gs_grav_val, name=name, title=title, description=description, users=users, 
                             startDatetime=startDatetime, remarks=remarks, containers=containers)


name = "201902142ulFactor1"

title = "2ul Gravimetric Validation Factor 1"

description = """Gravimetric validation of pipetting accuracy of each tip of Genesis Liquid Handler for a volume of 5 x 2 ul in 100 ul with factor adjusted to 1.02"""

users = ["AWE","MDO"]

remarks = "remarks"

containers = []
    
gs_grav_2ul_f1 = Experiment(gs_grav_val, name=name, title=title, description=description, users=users, 
                            startDatetime=startDatetime, remarks=remarks, containers=containers)


name = "201902142ulFactor2"

title = "2ul Gravimetric Validation Factor 2"

description = """Gravimetric validation of pipetting accuracy of each tip of Genesis Liquid Handler for a volume of 5 x 2 ul in 100 ul with factor adjusted to 1.036"""

users = ["AWE","MDO"]

remarks = "remarks"
    
gs_grav_2ul_f2 = Experiment(gs_grav_val, name=name, title=title, description=description, users=users,
                             startDatetime=startDatetime, remarks=remarks, containers=containers)


name = "201902142ulFactor3"

title = "2ul Gravimetric Validation Factor 3"

description = """Gravimetric validation of pipetting accuracy of each tip of Genesis Liquid Handler for a volume of 5 x 2 ul in 100 ul with factor adjusted to 1.1"""

users = ["AWE","MDO"]

remarks = "remarks"

containers = []
    
gs_grav_2ul_f3 = Experiment(gs_grav_val, name=name, title=title, description=description, users=users, 
                            startDatetime=startDatetime, remarks=remarks, containers=containers)


name = "201902142ul3To15"

title = "2ul Gravimetric Validation Factor 3To15"

description = """Gravimetric validation of pipetting accuracy of each tip of Genesis Liquid Handler for a volume of 5 x 2 ul in 100 ul with liquid class parameters from Standard 3-15 ul"""

users = ["AWE","MDO"]

remarks = "remarks"

containers = []

gs_grav_2ul_3to15 = Experiment(gs_grav_val, name=name, title=title, description=description, users=users, 
                               startDatetime=startDatetime, remarks=remarks, containers=containers)


name = "201902142ulSpeedWaitTime"

title = "2ul Gravimetric Validation Speed Wait Time"

description = """Gravimetric validation of pipetting accuracy of each tip of Genesis Liquid Handler for a volume of 5 x 2 ul in 100 ul with aspiration speed decreased and wait time increased"""

users = ["AWE","MDO"]

remarks = "remarks"

containers = []
    
gs_grav_2ul_spwt = Experiment(gs_grav_val, name=name, title=title, description=description, users=users, 
                                startDatetime=startDatetime, remarks=remarks, containers=containers)


name = "HamiltonLiquidHandlerValidation"

title = "Hamilton Liquid Handler Validation"

description = """Validation of pipetting accuracy of Hamilton liquid handler"""

users = ["AWE","MDO"]

startDatetime = "2019-02-21T00:00:00"

remarks = "remarks"


ham_val = Project(lara_val, name=name, title=title, description=description, users=users, 
                    startDatetime=startDatetime, remarks=remarks)


name = "GravimetricValidation"

title = "Gravimetric Validation"

description = """Determination of pipetting accuracy of liquid handler in low and high range through measurement of liquid weight in well rows"""

users = ["AWE","MDO"]

remarks = "remarks"

ham_grav_val = Project(ham_val, name=name, title=title, description=description, users=users, 
                        startDatetime=startDatetime, remarks=remarks)


name = "2019022150ulTip50ulVolume"

title = "50 ul Tip 50 ul Volume"

description = """Pipetting of 50 ul water into first column of a 96 well plate with removable row strips and measurement of weight difference between empty and filled row strips"""

users = ["AWE","MDO"]

remarks = "remarks"

ham_grav_50t50v = Project(ham_grav_val, name=name, title=title, description=description, users=users, 
                             startDatetime=startDatetime, remarks=remarks)



name = "2019022150ulTip3ulVolume"

title = "50 ul Tip 3 ul Volume"

description = """Pipetting of 3 ul water into first column of a 96 well plate with removable row strips prefilled with 50 ul water and measurement of weight difference between empty and filled row strips"""

users = ["AWE","MDO"]

remarks = "remarks"

ham_grav_50t3v = Project(ham_grav_val, name=name, title=title, description=description, users=users, 
                         startDatetime=startDatetime, remarks=remarks)



name = "201902211000ulTip500ulVolume"

title = "1000 ul Tip 500 ul Volume"

description = """Pipetting of 500 ul water into 2 mL reaction tubes and measurement of weight difference between empty and filled tubes"""

users = ["AWE","MDO"]

remarks = "remarks"

ham_grav_1000t500v = Project(ham_grav_val, name=name, title=title, description=description, users=users, 
                      startDatetime=startDatetime, remarks=remarks)


name = "201902211000ulTip20ulVolume"

title = "1000 ul Tip 20 ul Volume"

description = """Pipetting of 20 ul water into first column of a 96 well plate with removable row strips and measurement of weight difference between empty and filled row strips"""

users = ["AWE","MDO"]

remarks = "remarks"

ham_grav_1000t20v = Project(ham_grav_val, name=name, title=title, description=description, users=users, 
                             startDatetime=startDatetime, remarks=remarks)



name = "2019022150ulTip3ulVolumeSurface"

title = "50 ul Tip 3 ul Volume Surface"

description = """Pipetting of 3 ul water into first column of a 96 well plate with removable row strips and measurement of weight difference between empty and filled row strips. Dispension mode was surface empty tip instead of jet empty tip"""

users = ["AWE","MDO"]

remarks = "remarks"

ham_grav_50t3v_su = Project(ham_grav_val, name=name, title=title, description=description, users=users, 
                             startDatetime=startDatetime, remarks=remarks)


name = "2019022150ulTip3ulVolumeSideTouch"

title = "50 ul Tip 3 ul Volume Side Touch"

description = """Pipetting of 3 ul water into first column of a 96 well plate with removable row strips and measurement of weight difference between empty and filled row strips. Dispension mode was side touch instead of fixed height"""

users = ["AWE","MDO"]

remarks = "remarks"

ham_grav_50t3v_st = Project(ham_grav_val, name=name, title=title, description=description, users=users, 
                              startDatetime=startDatetime, remarks=remarks)


name = "2019022250ulTip50ulVolumeSideTouch"

title = "50 ul Tip 50 ul Volume"

description = """Pipetting of 50 ul water into first column of a 96 well plate with removable row strips and measurement of weight difference between empty and filled row strips. Dispension mode was side touch instead of fixed height"""

users = ["AWE","MDO"]

startDatetime = "2019-02-22T00:00:00"

remarks = "remarks"

ham_grav_50t50v_st = Project(ham_grav_val, name=name, title=title, description=description, users=users, 
                                startDatetime=startDatetime, remarks=remarks)





