"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_projects admin *

:details: lara_django_projects admin module admin backend configuration.
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - run "lara-django-dev tables_generator lara_django_projects >> tables.py" to update this file
________________________________________________________________________
"""
# django Tests s. https://docs.djangoproject.com/en/4.1/topics/testing/overview/ for lara_django_projects
# generated with django-extensions tests_generator  lara_django_projects > tests.py (LARA-version)

import logging
import django_tables2 as tables


from .models import Experiment, Project, ExperimentSelection, ProjSelection, ProjectsCalendar, ExperimentCalendar, OutputTemplate, Report, ProjectSynchronisationSettings

class ExperimentTable(tables.Table):
    # adding link to column <column-to-be-linked>
    title = tables.Column(linkify=('lara_django_projects:experiment-detail', [tables.A('pk')]))

    class Meta:
        model = Experiment

        fields = (
                'title',
                'name_full',
                'namespace',
                
                
                'parent',
               
               
                'datetime_start',
                'datetime_added',
                'status',
                'outcome',
               
                'reference_sample_source',
                'parameters',
                'reference_experiment',
               
                'observations',
                'description',
                'remarks',
               )

class ProjectTable(tables.Table):
    # adding link to column <column-to-be-linked>
    title = tables.Column(linkify=('lara_django_projects:project-detail', [tables.A('pk')]))

    class Meta:
        model = Project

        fields = (
                'title',
                'namespace',
                'name_full',
               
                'parent',
               
                'datetime_start',
                'datetime_added',
                'status',
                'status_outcome',
               
               
                'description',
                'remarks',
               )

class ExperimentSelectionTable(tables.Table):
    # adding link to column <column-to-be-linked>
    name = tables.Column(linkify=('lara_django_projects:experimentselection-detail', [tables.A('pk')]))

    class Meta:
        model = ExperimentSelection

        fields = (
                'name',
                'user')

class ProjSelectionTable(tables.Table):
    # adding link to column <column-to-be-linked>
    name = tables.Column(linkify=('lara_django_projects:projselection-detail', [tables.A('pk')]))

    class Meta:
        model = ProjSelection

        fields = (
                'name',
                'user')

class ProjectsCalendarTable(tables.Table):
    # adding link to column <column-to-be-linked>
    title = tables.Column(linkify=('lara_django_projects:projectscalendar-detail', [tables.A('pk')]))

    class Meta:
        model = ProjectsCalendar

        fields = (
                'title',
                'calendar_URL',
                'summary',
                'description',
                'color',
                'ics',
                'start_datetime',
                'end_datetime',
                'duration',
                'all_day',
                'created',
                'last_modified',
                'timestamp',
                'location',
                'geolocation',
                'conference_URL',
                'range',
                'related',
                'role',
                'tzid',
                'offset_UTC',
                'alarm_repeat_JSON',
                'event_repeat',
                'event_repeat_JSON',
                'user',
                'project')

class ExperimentCalendarTable(tables.Table):
    # adding link to column <column-to-be-linked>
    title = tables.Column(linkify=('lara_django_projects:experimentcalendar-detail', [tables.A('pk')]))

    class Meta:
        model = ExperimentCalendar

        fields = (
                'title',
                'calendar_URL',
                'summary',
                'description',
                'color',
                'ics',
                'start_datetime',
                'end_datetime',
                'duration',
                'all_day',
                'created',
                'last_modified',
                'timestamp',
                'location',
                'geolocation',
                'conference_URL',
                'range',
                'related',
                'role',
                'tzid',
                'offset_UTC',
                'alarm_repeat_JSON',
                'event_repeat',
                'event_repeat_JSON',
                'user',
                'experiment')

class OutputTemplateTable(tables.Table):
    # adding link to column <column-to-be-linked>
    name = tables.Column(linkify=('lara_django_projects:outputtemplate-detail', [tables.A('pk')]))

    class Meta:
        model = OutputTemplate

        fields = (
                'namespace',
                'name',
                'description',
                'format',
                'template_text',
                'file',
                'datetime_created',
                'datetime_updated',
                'version')

class ReportTable(tables.Table):
    # adding link to column <column-to-be-linked>
    name = tables.Column(linkify=('lara_django_projects:report-detail', [tables.A('pk')]))

    class Meta:
        model = Report

        fields = (
                'name',
                'IRI',
                'handle',
                'description',
                'template',
                'format',
                'text',
                'file',
                'PDF',
                'datetime_created',
                'datetime_updated')

class ProjectSynchronisationSettingsTable(tables.Table):
    # adding link to column <column-to-be-linked>
    name = tables.Column(linkify=('lara_django_projects:projectsynchronisationsettings-detail', [tables.A('pk')]))

    class Meta:
        model = ProjectSynchronisationSettings

        fields = (
                'name',
                'target_server',
                'login',
                'pass_wd',
                'auth_key',
                'cert',
                'datetime_started',
                'duration',
                'event_repeat',
                'event_repeat_JSON',
                'logs',
                'description')

