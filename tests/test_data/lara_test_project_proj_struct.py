# root project

name = "LARAvalidation"
title = "LARA Robot Validation"
description = """LARA Robot validation experiments to evaluate, document and monitor the performance 
of the Greifswald robotic high-troughput protein engineering platform LARA """
user = ["AWE","MDO"]
startDatetime = "2018-12-07 12:21"
remarks = "remarks"
lara_val = Project(None,parent_project="root", name=name, title=title, description=description, 
                   user=user, startDatetime=startDatetime, remarks=remarks)


# subprojects

name = "LiquidHandlerValidation"
title = "Liquid Handler Validation"
description = """Determination of a feasible system for liquid handler validation"""

lh_val = Project(lara_val, name=name, title=title, description=description, 
                           user=user, startDatetime=startDatetime, remarks=remarks)


name = "EvaporationControl"
title = "Evaporation Control"
description = """Determination of evaporation rates for different volumes of water and 1mM and 10 mM NaCl solutions"""

lh_evap_crtl= Project(lh_val, name=name, title=title, description=description, 
                              user=user, startDatetime=startDatetime, remarks=remarks)


name = "GravimetricValidation"
title = "Gravimetric Validation"
description = """Determination of pipetting accuracy of pipetting robot in lower/mid/high range through measurement of liquid weight in well columns and rows"""
startDatetime = "2018-12-12 12:21"


lh_grav_val = Project(lh_val, name=name, title=title, description=description, user=user, startDatetime=startDatetime, remarks=remarks)

name = "2018-12-12 12:212ulInEmptyCol"
title = "2uL In Empty Column Plate Well"
description = """Pipetting of 2ul water into each well of a 96 well plate with removable column strips and measurement of weight difference between empty and filled column strips"""

lh_grav_2ulcol = Experiment(lh_grav_val, name=name, title=title, description=description, user=user, startDatetime=startDatetime, remarks=remarks)


name = "2018-12-12 12:212ulInEmptyColRev"
title = "2uL In Empty Column Plate Well Reverse"
description = """Pipetting of 2ul water into each well of a 96 well plate with removable column strips in reversed alignment and measurement of weight difference between empty and filled column strips"""
user = ["AWE","MDO"]
startDatetime = "2018-12-12 12:21"
remarks = "remarks"

lh_grav_2ulcolrev = Experiment(lh_grav_val, name=name, title=title, description=description, user=user, startDatetime=startDatetime, remarks=remarks)
name = "OneStepPipetting"
title = "One Step Pipetting"
description = """Simult. precision and accuracy measurements utilizing a dual dye system"""
startDatetime = "2018-12-14 12:21"

lh_onestep_val = Project(lh_val, name=name, title=title, description=description, 
                                 user=user, startDatetime=startDatetime, remarks=remarks)


name = "DualDyeCoClNiSO"
title = "Dye Ratio CoCl2 NiSO4"
description = """Dual dye measurements with CoCl2 and NiSO4 dye pair"""

dd_cocl_niso = Project(lh_onestep_val, name=name, title=title, description=description, 
                                       user=user, startDatetime=startDatetime, remarks=remarks)


name = "2018-12-14 12:21DualDyeCoClNiSODilutionSeries"
title = "Dye Ratio CoCl2 NiSO4 Dilution Series"
description = """Dilution series with CoCl2 and NiSO4 dye pair at final volumes of 200, 150 and 100 uL"""
user = ["AWE","MDO"]
containers = "VA18121402"

ds_cocl_niso = Experiment(dd_cocl_niso, name=name, title=title, description=description, 
                                        user=user, startDatetime=startDatetime, remarks=remarks, containers=containers)
                                        

name = "GravimetricValidation"
title = "Gravimetric Validation"
description = """Determination of pipetting accuracy of pipetting robot in lower/mid/high range through measurement of liquid weight in well columns and rows"""
lh_grav_val = Project(lh_val, name=name, title=title, description=description, user=user, startDatetime=startDatetime, remarks=remarks)


name = "201812122ulInEmptyCol"
title = "2uL In Empty Column Plate Well"
description = """Pipetting of 2ul water into each well of a 96 well plate with removable column strips and measurement of weight difference between empty and filled column strips"""
lh_grav_2ulcol = Experiment(lh_grav_val, name=name, title=title, description=description, user=user, startDatetime=startDatetime, remarks=remarks)


name = "201812122ulInEmptyColRev"
title = "2uL In Empty Column Plate Well Reverse"
description = """Pipetting of 2ul water into each well of a 96 well plate with removable column strips in reversed alignment and measurement of weight difference between empty and filled column strips"""
lh_grav_2ulcolrev = Experiment(lh_grav_val, name=name, title=title, description=description, user=user, startDatetime=startDatetime, remarks=remarks)

ds_cocl_niso.printNamesRec()
ds_cocl_niso.addDBRec()
