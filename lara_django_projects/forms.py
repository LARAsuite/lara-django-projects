"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_projects admin *

:details: lara_django_projects admin module admin backend configuration.
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - run "lara-django-dev forms_generator -c lara_django_projects > forms.py" to update this file
________________________________________________________________________
"""
# django crispy forms s. https://github.com/django-crispy-forms/django-crispy-forms for []
# generated with django-extensions forms_generator -c  [] > forms.py (LARA-version)

from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, Row, Column

from .models import Experiment, Project, ExperimentSelection, ProjSelection, ProjectsCalendar, ExperimentCalendar, OutputTemplate, Report, ProjectSynchronisationSettings


class ExperimentCreateForm(forms.ModelForm):
    class Meta:
        model = Experiment
        fields = (
            'namespace',
            'name',
            'name_full',
            'title',
            'parent',
            'experiment_design',
            'hash_SHA256',
            'UUID',
            'datetime_start',
            'datetime_added',
            'status',
            'outcome',
            'view_selected',
            'view_collapsed',
            'reference_sample_source',
            'parameters',
            'reference_experiment',
            'literature',
            'observations',
            'description',
            'remarks',
            'IRI',
            'handle',
            )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'namespace',
            'name',
            'name_full',
            'title',
            'parent',
            'experiment_design',
            'hash_SHA256',
            'UUID',
            'datetime_start',
            'datetime_added',
            'status',
            'outcome',
            'view_selected',
            'view_collapsed',
            'reference_sample_source',
            'parameters',
            'reference_experiment',
            'literature',
            'observations',
            'description',
            'remarks',
            'IRI',
            'handle',
            
            Submit('submit', 'Create')
        )


class ExperimentUpdateForm(forms.ModelForm):
    class Meta:
        model = Experiment
        fields = (
            'namespace',
            'name',
            'name_full',
            'title',
            'parent',
            'experiment_design',
            'hash_SHA256',
            'UUID',
            'datetime_start',
            'datetime_added',
            'status',
            'outcome',
            'view_selected',
            'view_collapsed',
            'reference_sample_source',
            'parameters',
            'reference_experiment',
            'literature',
            'observations',
            'description',
            'remarks',
            'IRI',
            'handle',
            )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'namespace',
            'name',
            'name_full',
            'title',
            'parent',
            'experiment_design',
            'hash_SHA256',
            'UUID',
            'datetime_start',
            'datetime_added',
            'status',
            'outcome',
            'view_selected',
            'view_collapsed',
            'reference_sample_source',
            'parameters',
            'reference_experiment',
            'literature',
            'observations',
            'description',
            'remarks',
            'IRI',
            'handle',
            Submit('submit', 'Create')
        )


class ProjectCreateForm(forms.ModelForm):
    class Meta:
        model = Project
        fields = (
            'namespace',
            'name',
            'name_full',
            'title',
            'parent',
            'hash_SHA256',
            'UUID',
            'datetime_start',
            'datetime_added',
            'status',
            'status_outcome',
            'view_selected',
            'view_collapsed',
            'literature',
            'description',
            'remarks',
            'IRI',
            'handle',
            )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'namespace',
            'name',
            'name_full',
            'title',
            'parent',
            'hash_SHA256',
            'UUID',
            'datetime_start',
            'datetime_added',
            'status',
            'status_outcome',
            'view_selected',
            'view_collapsed',
            'literature',
            'description',
            'remarks',
            'IRI',
            'handle',
            
            Submit('submit', 'Create')
        )


class ProjectUpdateForm(forms.ModelForm):
    class Meta:
        model = Project
        fields = (
            'namespace',
            'name',
            'name_full',
            'title',
            'parent',
            'hash_SHA256',
            'UUID',
            'datetime_start',
            'datetime_added',
            'status',
            'status_outcome',
            'view_selected',
            'view_collapsed',
            'literature',
            'description',
            'remarks',
            'IRI',
            'handle',
            )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'namespace',
            'name',
            'name_full',
            'title',
            'parent',
            'hash_SHA256',
            'UUID',
            'datetime_start',
            'datetime_added',
            'status',
            'status_outcome',
            'view_selected',
            'view_collapsed',
            'literature',
            'description',
            'remarks',
            'IRI',
            'handle',
            
            Submit('submit', 'Create')
        )


class ExperimentSelectionCreateForm(forms.ModelForm):
    class Meta:
        model = ExperimentSelection
        fields = (
            'name',
            'user')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'name',
            'user',
            Submit('submit', 'Create')
        )


class ExperimentSelectionUpdateForm(forms.ModelForm):
    class Meta:
        model = ExperimentSelection
        fields = (
            'name',
            'user')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'name',
            'user',
            Submit('submit', 'Create')
        )


class ProjSelectionCreateForm(forms.ModelForm):
    class Meta:
        model = ProjSelection
        fields = (
            'name',
            'user')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'name',
            'user',
            Submit('submit', 'Create')
        )


class ProjSelectionUpdateForm(forms.ModelForm):
    class Meta:
        model = ProjSelection
        fields = (
            'name',
            'user')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'name',
            'user',
            Submit('submit', 'Create')
        )


class ProjectsCalendarCreateForm(forms.ModelForm):
    class Meta:
        model = ProjectsCalendar
        fields = (
            'title',
            'calendar_URL',
            'summary',
            'description',
            'color',
            'ics',
            'start_datetime',
            'end_datetime',
            'duration',
            'all_day',
            'created',
            'last_modified',
            'timestamp',
            'location',
            'geolocation',
            'conference_URL',
            'range',
            'related',
            'role',
            'tzid',
            'offset_UTC',
            'alarm_repeat_JSON',
            'event_repeat',
            'event_repeat_JSON',
            'user',
            'project')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'title',
            'calendar_URL',
            'summary',
            'description',
            'color',
            'ics',
            'start_datetime',
            'end_datetime',
            'duration',
            'all_day',
            'created',
            'last_modified',
            'timestamp',
            'location',
            'geolocation',
            'conference_URL',
            'range',
            'related',
            'role',
            'tzid',
            'offset_UTC',
            'alarm_repeat_JSON',
            'event_repeat',
            'event_repeat_JSON',
            'user',
            'project',
            Submit('submit', 'Create')
        )


class ProjectsCalendarUpdateForm(forms.ModelForm):
    class Meta:
        model = ProjectsCalendar
        fields = (
            'title',
            'calendar_URL',
            'summary',
            'description',
            'color',
            'ics',
            'start_datetime',
            'end_datetime',
            'duration',
            'all_day',
            'created',
            'last_modified',
            'timestamp',
            'location',
            'geolocation',
            'conference_URL',
            'range',
            'related',
            'role',
            'tzid',
            'offset_UTC',
            'alarm_repeat_JSON',
            'event_repeat',
            'event_repeat_JSON',
            'user',
            'project')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'title',
            'calendar_URL',
            'summary',
            'description',
            'color',
            'ics',
            'start_datetime',
            'end_datetime',
            'duration',
            'all_day',
            'created',
            'last_modified',
            'timestamp',
            'location',
            'geolocation',
            'conference_URL',
            'range',
            'related',
            'role',
            'tzid',
            'offset_UTC',
            'alarm_repeat_JSON',
            'event_repeat',
            'event_repeat_JSON',
            'user',
            'project',
            Submit('submit', 'Create')
        )


class ExperimentCalendarCreateForm(forms.ModelForm):
    class Meta:
        model = ExperimentCalendar
        fields = (
            'title',
            'calendar_URL',
            'summary',
            'description',
            'color',
            'ics',
            'start_datetime',
            'end_datetime',
            'duration',
            'all_day',
            'created',
            'last_modified',
            'timestamp',
            'location',
            'geolocation',
            'conference_URL',
            'range',
            'related',
            'role',
            'tzid',
            'offset_UTC',
            'alarm_repeat_JSON',
            'event_repeat',
            'event_repeat_JSON',
            'user',
            'experiment')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'title',
            'calendar_URL',
            'summary',
            'description',
            'color',
            'ics',
            'start_datetime',
            'end_datetime',
            'duration',
            'all_day',
            'created',
            'last_modified',
            'timestamp',
            'location',
            'geolocation',
            'conference_URL',
            'range',
            'related',
            'role',
            'tzid',
            'offset_UTC',
            'alarm_repeat_JSON',
            'event_repeat',
            'event_repeat_JSON',
            'user',
            'experiment',
            Submit('submit', 'Create')
        )


class ExperimentCalendarUpdateForm(forms.ModelForm):
    class Meta:
        model = ExperimentCalendar
        fields = (
            'title',
            'calendar_URL',
            'summary',
            'description',
            'color',
            'ics',
            'start_datetime',
            'end_datetime',
            'duration',
            'all_day',
            'created',
            'last_modified',
            'timestamp',
            'location',
            'geolocation',
            'conference_URL',
            'range',
            'related',
            'role',
            'tzid',
            'offset_UTC',
            'alarm_repeat_JSON',
            'event_repeat',
            'event_repeat_JSON',
            'user',
            'experiment')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'title',
            'calendar_URL',
            'summary',
            'description',
            'color',
            'ics',
            'start_datetime',
            'end_datetime',
            'duration',
            'all_day',
            'created',
            'last_modified',
            'timestamp',
            'location',
            'geolocation',
            'conference_URL',
            'range',
            'related',
            'role',
            'tzid',
            'offset_UTC',
            'alarm_repeat_JSON',
            'event_repeat',
            'event_repeat_JSON',
            'user',
            'experiment',
            Submit('submit', 'Create')
        )


class OutputTemplateCreateForm(forms.ModelForm):
    class Meta:
        model = OutputTemplate
        fields = (
            'namespace',
            'name',
            'description',
            'format',
            'template_text',
            'file',
            'datetime_created',
            'datetime_updated',
            'version')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'namespace',
            'name',
            'description',
            'format',
            'template_text',
            'file',
            'datetime_created',
            'datetime_updated',
            'version',
            Submit('submit', 'Create')
        )


class OutputTemplateUpdateForm(forms.ModelForm):
    class Meta:
        model = OutputTemplate
        fields = (
            'namespace',
            'name',
            'description',
            'format',
            'template_text',
            'file',
            'datetime_created',
            'datetime_updated',
            'version')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'namespace',
            'name',
            'description',
            'format',
            'template_text',
            'file',
            'datetime_created',
            'datetime_updated',
            'version',
            Submit('submit', 'Create')
        )


class ReportCreateForm(forms.ModelForm):
    class Meta:
        model = Report
        fields = (
            'name',
            'IRI',
            'handle',
            'description',
            'template',
            'format',
            'text',
            'file',
            'PDF',
            'datetime_created',
            'datetime_updated')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'name',
            'IRI',
            'handle',
            'description',
            'template',
            'format',
            'text',
            'file',
            'PDF',
            'datetime_created',
            'datetime_updated',
            Submit('submit', 'Create')
        )


class ReportUpdateForm(forms.ModelForm):
    class Meta:
        model = Report
        fields = (
            'name',
            'IRI',
            'handle',
            'description',
            'template',
            'format',
            'text',
            'file',
            'PDF',
            'datetime_created',
            'datetime_updated')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'name',
            'IRI',
            'handle',
            'description',
            'template',
            'format',
            'text',
            'file',
            'PDF',
            'datetime_created',
            'datetime_updated',
            Submit('submit', 'Create')
        )


class ProjectSynchronisationSettingsCreateForm(forms.ModelForm):
    class Meta:
        model = ProjectSynchronisationSettings
        fields = (
            'name',
            'target_server',
            'login',
            'pass_wd',
            'auth_key',
            'cert',
            'datetime_started',
            'duration',
            'event_repeat',
            'event_repeat_JSON',
            'logs',
            'description')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'name',
            'target_server',
            'login',
            'pass_wd',
            'auth_key',
            'cert',
            'datetime_started',
            'duration',
            'event_repeat',
            'event_repeat_JSON',
            'logs',
            'description',
            Submit('submit', 'Create')
        )


class ProjectSynchronisationSettingsUpdateForm(forms.ModelForm):
    class Meta:
        model = ProjectSynchronisationSettings
        fields = (
            'name',
            'target_server',
            'login',
            'pass_wd',
            'auth_key',
            'cert',
            'datetime_started',
            'duration',
            'event_repeat',
            'event_repeat_JSON',
            'logs',
            'description')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'name',
            'target_server',
            'login',
            'pass_wd',
            'auth_key',
            'cert',
            'datetime_started',
            'duration',
            'event_repeat',
            'event_repeat_JSON',
            'logs',
            'description',
            Submit('submit', 'Create')
        )

# from .forms import ExperimentCreateForm, ProjectCreateForm, ExperimentSelectionCreateForm, ProjSelectionCreateForm, ProjectsCalendarCreateForm, ExperimentCalendarCreateForm, OutputTemplateCreateForm, ReportCreateForm, ProjectSynchronisationSettingsCreateFormExperimentUpdateForm, ProjectUpdateForm, ExperimentSelectionUpdateForm, ProjSelectionUpdateForm, ProjectsCalendarUpdateForm, ExperimentCalendarUpdateForm, OutputTemplateUpdateForm, ReportUpdateForm, ProjectSynchronisationSettingsUpdateForm
