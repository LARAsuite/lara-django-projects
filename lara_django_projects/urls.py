"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_projects urls *

:details: lara_django_projects urls module.
         - add app specific urls here
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: -
________________________________________________________________________
"""

from . import views
from django.conf import settings
from django.conf.urls.static import static
from django.urls import path, include
from django.views.generic import TemplateView
from django_socio_grpc.settings import grpc_settings
#import lara_django.urls as base_urls

from . import views

# Add your {cookiecutter.project_slug}} urls here.


# !! this sets the apps namespace to be used in the template
app_name = "lara_django_projects"

# companies and institutions should also be added
# the 'name' attribute is used in templates to address the url independent of the view


urlpatterns = [
    # the 'name' value as called by the {% url %} template tag
    path('project/overview', views.RootIndexView.as_view(), name='project-overview'),
    path('project/tree/<uuid:pk>/', views.ProjectTree.as_view(), name='project-tree'),
    path('project/list/', views.ProjectSingleTableView.as_view(), name='project-list'),
    path('project/create/', views.ProjectCreateView.as_view(), name='project-create'),
    path('project/update/<uuid:pk>', views.ProjectUpdateView.as_view(), name='project-update'),
    path('project/delete/<uuid:pk>', views.ProjectDeleteView.as_view(), name='project-delete'),
    path('project/<uuid:pk>/', views.ProjectDetailView.as_view(), name='project-detail'),

    path('experiment/list/', views.ExperimentSingleTableView.as_view(), name='experiment-list'),
    path('experiment/create/', views.ExperimentCreateView.as_view(), name='experiment-create'),
    path('experiment/update/<uuid:pk>', views.ExperimentUpdateView.as_view(), name='experiment-update'),
    path('experiment/delete/<uuid:pk>', views.ExperimentDeleteView.as_view(), name='experiment-delete'),
    path('experiment/<uuid:pk>/', views.ExperimentDetailView.as_view(), name='experiment-detail'),

   
    path('details/<uuid:pk>/', views.ProjectDetails.as_view(), name='project-details'),
    #path('<uuid:pk>/', views.ProjectTree.as_view(), name='project-tree'),
    #path('experiments/<uuid:pk>/', views.ExperimentDetails.as_view(),
    #     name='experiment-details'),

    
    path('', views.ProjectSingleTableView.as_view(), name='project-root'),

    # ~ path('^projectslist$', views.projectsList, name='projects-list'), # projects as a long list/table
    #~ path('search/', views.searchForm, name='searchForm'),
    #~ path('search-results/', views.search, name='search'),
    #~ path('results/', views.results, name='results'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


# urlpatterns = [
#     path('', views.EntitySingleTableView.as_view(), name='entity-list-root'),
#     path('entities/list/', views.EntitySingleTableView.as_view(), name='entity-list'),
#     path('addresses/list/', views.AddressesListView.as_view(), name='address-list'),
#     path('addresses/create/', views.AddressCreateView.as_view(),
#          name='address-create'),
#     path('addresses/update/<uuid:pk>', views.AddressUpdateView.as_view(),
#          name='address-update'),
#     path('addresses/delete/<uuid:pk>', views.AddressDeleteView.as_view(),
#          name='address-delete'),
# ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

# register handlers in settings
grpc_settings.user_settings["GRPC_HANDLERS"] = [
    "lara_django_projects.grpc_if.handlers.grpc_handlers"]
