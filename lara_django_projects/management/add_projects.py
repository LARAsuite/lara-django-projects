"""_____________________________________________________________________

:PROJECT: LARA

*lara_projects models *

:details: lara_projects database models. 
         -

:file:    models.py
:authors: mark doerr (mark.doerr@uni-greifswald.de)

:date: (creation)          
:date: (last modification) 20190127

.. note:: -
.. todo:: - 
________________________________________________________________________
**Copyright**:
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

  For further Information see LICENSE file that comes with this distribution.
________________________________________________________________________
"""

__version__ = "0.1.5"

import os
import logging
import csv

from django.core.management.base import BaseCommand, CommandError

from lara_django_projects.management.add_projects_python import  AddPythonProjects2DB
#from lara_projects.management.add_projects_json import  AddJSONProjects2DB

class AddProjects2DB(BaseCommand):
    def __init__(self, filename=None, prettyPrint=False, replace_data=False):
        super(AddProjects2DB, self).__init__()
        
        self.replace_data = replace_data
        
        if self.replace_data:
            self.stderr.write(self.style.WARNING("Replacing data in db !!!"))
        
        # parse filename
        filename_base, file_extension = os.path.splitext(filename)
        
        if file_extension == ".cvs" :
            """ not used yet, shall be later used for adding complex projects """ 
            try:
                with open(csv_filename, 'rb' ) as csvfile:
                    self.csv_reader = csv.DictReader(csvfile)
                    self.stdout.write(self.style.SUCCESS("now adding projects ..."))
                
            except ValueError as err :
                self.stderr.write('file %s, ValueError: %s' % (csv_filename, err ))
                exit()
            except KeyError as err :
                self.stderr.write('file %s, KeyError: %s\n Please check, if file is exported with quote delimited strings' % (csv_filename, err))
                exit()
            except Exception as err :
                self.stderr.write('file %s, Error: %s' % (csv_filename, err))
                exit()
        
        # calling the actual addion routines ...
        elif file_extension == ".py" :
            AddPythonProjects2DB(filename=filename, prettyPrint=prettyPrint, replace_data=replace_data)
            
        #~ elif file_extension == ".json" :
            #~ AddJSONProjects2DB(filename=filename, prettyPrint=prettyPrint, replace_data=replace_data)
       
    def handle(self, *args, **options):
        """ handle needs to be definded when subclassing BaseCommand """
        pass
        
    
