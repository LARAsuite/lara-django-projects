"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_projects tests *

:details: lara_django_projects application urls tests.
         - 
:authors: mark doerr  <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - 
________________________________________________________________________
"""

from django.test import TestCase
from django.urls import resolve, reverse

# from lara_django_projects.models import

# Create your lara_django_projects tests here.
