"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_projects models *

:details: lara_django_projects database models.
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - remove unwanted models/fields
________________________________________________________________________
"""

import logging
from datetime import datetime, timezone
import uuid
from random import randint

from django.utils import timezone
from django.conf import settings
from django.utils.translation import gettext_lazy as _

from django.db import models
from mptt.models import MPTTModel, TreeForeignKey

from lara_django_base.models import Tag, Namespace, ItemStatus, CalendarAbstract
from lara_django_people.models import Entity, LaraUser
#from lara_django_library.models import LibItem

from lara_django_data.models import Data, Evaluation

from lara_django_material_store.models  import PartInstance, DeviceInstance, LabwareInstance
from lara_django_substances_store.models import SubstanceInstance, PolymerInstance, MixtureInstance
from lara_django_organisms_store.models import OrganismInstance

from lara_django_processes.models import Method, Process, Procedure, \
                                         MethodInstance, ProcessInstance, ProcedureInstance

from lara_django_samples.models import Sample 

settings.FIXTURES += []


class Experiment(MPTTModel):
    """ this class stores all details of an LARA experiment"""
    # models.AutoField(primary_key=True)
    experiment_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    namespace = models.ForeignKey(Namespace, related_name='%(app_label)s_%(class)s_namespaces_related',
                                  related_query_name="%(app_label)s_%(class)s_namespaces_related_query",
                                  on_delete=models.CASCADE, null=True, blank=True,
                                  help_text="namespace of experiment")
    title = models.TextField(
        blank=True, help_text="human readable experiment title of the project")
    name = models.TextField(unique=True, blank=True,
                            help_text="name of the experiment")
    # autogenerate this:
    name_full = models.TextField(unique=True, blank=True,
                                 help_text="unique, fully qualified experiment name, should not contain whitespaces, used for human project descriptions, e.g. de.unigreifswald.biochem.akb.lara.validation")
   

    users = models.ManyToManyField(
        Entity, related_name="user_experiments", blank=True, help_text="experimentators")
    # -> ltree will be modelled with the PostgreSQL ltree https://pypi.org/project/django-ltree/
    # ~ parent = models.ForeignKey('self', null=True, blank=True, related_name="parent_projects" ,on_delete=models.CASCADE,
    # ~ help_text="parent experiments" )
    parent = TreeForeignKey('self', null=True, blank=True, related_name="%(app_label)s_%(class)s_experiment_parent",
                            related_query_name="%(app_label)s_%(class)s_experiment_parent_related_query",
                            on_delete=models.CASCADE,
                            help_text="mptt - parent experiments")
    experiment_design = models.ForeignKey(ProcessInstance, related_name='%(app_label)s_%(class)s_experiment_designs_related',
                                          related_query_name="%(app_label)s_%(class)s_experiment_designs_related_query",
                                          on_delete=models.CASCADE,
                                          null=True, blank=True,
                                          help_text="process/procedure describing the experimental design, e.g. via machine learning or DOI")
    hash_SHA256 = models.CharField(
        max_length=256, blank=True, null=True, help_text="SHA256 experiment hash")
    UUID = models.UUIDField(default=uuid.uuid4, help_text="experiment UUID")
    datetime_start = models.DateTimeField(default=datetime(1968, 12, 31, tzinfo=timezone.utc),
                                          help_text="start of experiment")  # auto_now_add=True or better default time = just an arbitrary date where each element is easy realisable from its value
    datetime_added = models.DateTimeField(default=datetime(1968, 12, 31, tzinfo=timezone.utc),
                                          help_text="experiment added to LARA")  # auto_now_add=True or better default time = just an arbitrary date where each element is easy realisable from its value
    status = models.ForeignKey(ItemStatus, related_name='%(app_label)s_%(class)s_status_related',
                               related_query_name="%(app_label)s_%(class)s_status_related_query", on_delete=models.CASCADE, null=True, blank=True,
                               help_text="experiment status classifier: planned, started, in_progess, finished")
    outcome = models.ForeignKey(ItemStatus, related_name='%(app_label)s_%(class)s_outcome_related',
                                related_query_name="%(app_label)s_%(class)s_outcome_related_query",
                                on_delete=models.CASCADE, null=True, blank=True,
                                help_text="experiment outcome classifier: preliminary, statistically_confirmed")
    view_selected = models.BooleanField(
        default=False, help_text="experiment selected view")
    view_collapsed = models.BooleanField(
        default=False, help_text="experiment view collapsed")
    # link back to projects ? might not be needed.
    # ~ projects = models.ManyToManyField(Experiments, related_name="experiment_projects", blank=True,
    # ~ help_text="link to all experiments of a project")
    data = models.ManyToManyField(Data, related_name='%(app_label)s_%(class)s_data_related',
                                  related_query_name="%(app_label)s_%(class)s_data_related_query", blank=True,
                                  help_text="link to all data of this experiment")
    evaluation = models.ManyToManyField(Evaluation, related_name='%(app_label)s_%(class)s_evalution_related',
                                        related_query_name="%(app_label)s_%(class)s_evaluation_related_query", blank=True,
                                        help_text="all evaluations of this experiment")

    method = models.ForeignKey(MethodInstance,on_delete=models.CASCADE, blank=True, null=True,
                               related_name='%(app_label)s_%(class)s_methods_related',
                                        related_query_name="%(app_label)s_%(class)s_methods_related_query",
                            help_text="method for measuring, should be replaced by lara_process.method")
    procedure = models.ForeignKey(ProcedureInstance, on_delete=models.CASCADE, blank=True, null=True, related_name='%(app_label)s_%(class)s_procedure_related',
                                        related_query_name="%(app_label)s_%(class)s_procedure_related_query", help_text="procedure")
    process = models.ForeignKey(ProcedureInstance, on_delete=models.CASCADE, blank=True, null=True, related_name='%(app_label)s_%(class)s_process_related',
                                        related_query_name="%(app_label)s_%(class)s_process_related_query", help_text="proccess")
    
    substances = models.ManyToManyField(SubstanceInstance, related_name='%(app_label)s_%(class)s_substance_related',
                                  related_query_name="%(app_label)s_%(class)s_substances_related_query", blank=True,
                                  help_text="substances used in experiment")
    
    polymers = models.ManyToManyField(PolymerInstance, related_name='%(app_label)s_%(class)s_polymers_related',
                                  related_query_name="%(app_label)s_%(class)s_polymers_related_query", blank=True,
                                  help_text="polymers used in experiment")
    
    mixtures = models.ManyToManyField(MixtureInstance, related_name='%(app_label)s_%(class)s_mixtures_related',
                                  related_query_name="%(app_label)s_%(class)s_mixtures_related_query", blank=True,
                                  help_text="mixtures used in experiment")
    
    organisms = models.ManyToManyField(OrganismInstance, related_name='%(app_label)s_%(class)s_organisms_related',
                                  related_query_name="%(app_label)s_%(class)s_organisms_related_query", blank=True,
                                  help_text="organisms used in experiment")

    reference_sample_source = models.TextField(blank=True, null=True,
                                               help_text="source barcode or reference and position from which sample originated")
    parts = models.ManyToManyField(PartInstance, related_name='%(app_label)s_%(class)s_parts_related',
                                  related_query_name="%(app_label)s_%(class)s_parts_related_query", blank=True,  help_text="parts used in experiment")
    devices = models.ManyToManyField(DeviceInstance, related_name='%(app_label)s_%(class)s_devices_related',
                                  related_query_name="%(app_label)s_%(class)s_devices_related_query", blank=True,  help_text="devices used in experiment")
    labwares = models.ManyToManyField(LabwareInstance, related_name='%(app_label)s_%(class)s_labware_related',
                                  related_query_name="%(app_label)s_%(class)s_labware_related_query", blank=True,  help_text="labware used in experiment")
    samples = models.ManyToManyField(Sample, related_name='%(app_label)s_%(class)s_samples_related',
                                  related_query_name="%(app_label)s_%(class)s_samples_related_query", blank=True,  help_text="samples used in experiment")
    # -> JSON should be JSON field
    # experimental parameters, like temperature, pH, ....
    parameters = models.JSONField(blank=True, null=True, help_text="experimental parameters in JSON format")
    # only one ref experiment ?
    reference_experiment = models.ForeignKey('self', related_name='%(app_label)s_%(class)s_reference_experiment_related',
                                             related_query_name="%(app_label)s_%(class)s_reference_experiment_related_query",
                                             on_delete=models.CASCADE, blank=True, null=True,
                                             help_text="pointer to experiment with reference data")
    literature = models.URLField(
        blank=True, null=True, help_text="literature and references")
    # literature = models.ManyToManyField(LibItem, related_name='%(app_label)s_%(class)s_literature_related',
    #                                     related_query_name="%(app_label)s_%(class)s_literature_related_query", blank=True,
    #                                     help_text="literature and references")
    tags = models.ManyToManyField(Tag, related_name='%(app_label)s_%(class)s_tags_related',
                                  related_query_name="%(app_label)s_%(class)s_tags_related_query", blank=True,
                                  help_text="experiment tags, for easier identification")
    observations = models.TextField(
        blank=True, null=True, help_text="observations during the experiment")
    description = models.TextField(
        blank=True, help_text="experiment description")
    remarks = models.TextField(
        blank=True, null=True, help_text="remarks to the experiment")
    IRI = models.URLField(
        blank=True,  null=True, unique=True, max_length=512, help_text="International Resource Identifier - IRI: is used for semantic representation ")
    handle = models.URLField(
        blank=True, null=True,  unique=True, max_length=512, help_text=" [handle URI](https://www.handle.net/)")

    # this feature needs to be used with care (if necessary)
    # ~ class MPTTMeta:
    # ~ order_insertion_by = ['title']

    def save(self, *args, **kwargs):
        """
        Here we generate some default values for name_full
        """
        if self.name is None or self.name == '':
            self.name = self.title.strip().replace(' ', '-').lower()
        

        if self.name_full is None or self.name_full == '':
            self.name_full = '/'.join(
                (self.namespace.name.strip(), self.name))
            
        super().save(*args, **kwargs)

    def __str__(self):
        return self.name or ''

    def __repr__(self):
        return self.name or ''


class Project(MPTTModel):
    """ this class stores all details of a project/experiment
    - mind the time when data and evaluations are added: data might be added before an evaluation has been defined and vice versa
    - finding related experiments: just look at children of an exp in tree or exp, that have this as parent
    - some of the fields are only used by experiments, 
    todo:
      - change device to devices
    """

    # models.AutoField(primary_key=True)
    project_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    namespace = models.ForeignKey(Namespace, related_name='%(app_label)s_%(class)s_namespaces_related',
                                  related_query_name="%(app_label)s_%(class)s_namespaces_related_query",
                                  on_delete=models.CASCADE, null=True, blank=True,
                                  help_text="namespace of project")
    title = models.TextField(
        blank=True, help_text="human readable project title of the project")
    # name is obsolete, only name_full is relevant
    name = models.TextField(blank=True,
                            help_text="name of the project")
    # autogenerate this:
    name_full = models.TextField(unique=True, blank=True,
                                 help_text="unique, fully qualified project name, should not contain whitespaces, used for human project descriptions, e.g. de.unigreifswald.biochem.akb.lara.validation")
   

    users = models.ManyToManyField(
        Entity, related_name="%(app_label)s_%(class)s_users_related",
        related_query_name="%(app_label)s_%(class)s_users_related_query", blank=True, help_text="project designers")
    # -> ltree,  will be modelled with the PostgreSQL ltree https://pypi.org/project/django-ltree/
    # ~ parent = models.ForeignKey('self', null=True, blank=True, related_name="parent_projects" ,on_delete=models.CASCADE,
    # ~ help_text="parent projects" )
    parent = TreeForeignKey('self', null=True, blank=True, related_name="%(app_label)s_%(class)s_experiment_parent_related",
                            related_query_name="%(app_label)s_%(class)s_parent_related_query",
                            on_delete=models.CASCADE,
                            help_text="mptt - parent projects")
    hash_SHA256 = models.CharField(
        max_length=256, blank=True, null=True, help_text="SHA256 project hash")
    UUID = models.UUIDField(default=uuid.uuid4, help_text="project UUID")
    datetime_start = models.DateTimeField(default=datetime(1968, 12, 31, tzinfo=timezone.utc),
                                          help_text="start of projects")  # auto_now_add=True or better default time = just an arbitrary date where each element is easy realisable from its value
    datetime_added = models.DateTimeField(default=datetime(1968, 12, 31, tzinfo=timezone.utc),
                                          help_text="project added to LARA")  # auto_now_add=True or better default time = just an arbitrary date where each element is easy realisable from its value
    status = models.ForeignKey(ItemStatus, related_name="%(app_label)s_%(class)s_status_related",
                               related_query_name="%(app_label)s_%(class)s_status_related_query", on_delete=models.CASCADE, null=True, blank=True,
                               help_text="project status classifier: planned, started, in_progress, finished")
    status_outcome = models.ForeignKey(ItemStatus, related_name="%(app_label)s_%(class)s_outcome_related",
                                       related_query_name="%(app_label)s_%(class)s_outcome_related_query",
                                       on_delete=models.CASCADE, null=True, blank=True,
                                       help_text="project outcome classifier: preliminary, statistically_confirmed")
    view_selected = models.BooleanField(
        default=False, help_text="project selected view")
    view_collapsed = models.BooleanField(
        default=False, help_text="project view collapsed")
    experiments = models.ManyToManyField(Experiment, related_name="%(app_label)s_%(class)s_experiments_related",
                                         related_query_name="%(app_label)s_%(class)s_experiments_related_query", blank=True,
                                         help_text="link to all experiments of a project")
    data = models.ManyToManyField(Data, related_name="%(app_label)s_%(class)s_data_related",
                                  related_query_name="%(app_label)s_%(class)s_data_related_query", blank=True,
                                  help_text="link to all measured data of this project")
    evaluation = models.ManyToManyField(Evaluation, related_name="%(app_label)s_%(class)s_evaluation_related",
                                        related_query_name="%(app_label)s_%(class)s_evaluation_related_query", blank=True,
                                        help_text="all evaluations of this project")
    literature = models.URLField(
        blank=True, null=True, help_text="literature and references")
    # literature = models.ManyToManyField(LibItem, related_name='%(app_label)s_%(class)s_literature_related',
    #                                     related_query_name="%(app_label)s_%(class)s_literature_related_query", blank=True,
    #                                     help_text="literature and references")
    tags = models.ManyToManyField(Tag, related_name='%(app_label)s_%(class)s_tags_related',
                                  related_query_name="%(app_label)s_%(class)s_tags_related_query",
                                  blank=True, help_text="project tags, for easier identification")
    description = models.TextField(blank=True, help_text="project description")
    remarks = models.TextField(
        blank=True, null=True, help_text="remarks to the project")
    IRI = models.URLField(
        blank=True,  null=True, unique=True, max_length=512, help_text="International Resource Identifier - IRI: is used for semantic representation ")
    handle = models.URLField(
        blank=True, null=True,  unique=True, max_length=512, help_text=" [handle URI](https://www.handle.net/)")
    

    def save(self, *args, **kwargs):
        """
        Here we generate some default values for name_full
        """
        if self.name is None or self.name == '':
            self.name = self.title.strip().replace(' ', '-').lower()

        if self.name_full is None or  self.name_full == '':
            self.name_full = '/'.join(
                (self.namespace.name.strip(), self.name))
            
        super().save(*args, **kwargs)


    def __str__(self):
        return self.name or ''

    def __repr__(self):
        return self.name or ''

    # this feature needs to be used with care (if necessary)
    # ~ class MPTTMeta:
        # ~ order_insertion_by = ['title']

    # ~ @my_date.setter
    # ~ def my_date(self, value):
        # ~ if value > datetime.date.today():
        #~ logger.warning("The date chosen was in the future.")
        # ~ self._my_date = value
    # ~ class Meta:
        # ~ db_table = "lara_projects"


class ExperimentSelection(models.Model):
    """ this class is used for selecting Experiment items with certain criteria  """

    experiment_sel_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    name = models.TextField(
        unique=True, help_text="unique experiment selection name/label")
    user = models.ForeignKey(LaraUser, related_name="%(app_label)s_%(class)s_user_related",
                             related_query_name="%(app_label)s_%(class)s_user_related_query", on_delete=models.CASCADE, null=True, blank=True,
                             help_text="LaraUser who created the selection")
    selected_items = models.ManyToManyField(
        Experiment, related_name="%(app_label)s_%(class)s_selected_items_related",
        related_query_name="%(app_label)s_%(class)s_selected_items_related_query", blank=True, help_text="selected experiments")

    def __str__(self):
        return self.name or ''

    def __repr__(self) -> str:
        return self.name or ''


class ProjSelection(models.Model):
    """ this class is used for selecting project items with certain criteria  """
    project_sel_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)  # models.AutoField(primary_key=True)
    name = models.TextField(
        unique=True, help_text="unique project selection name/label")
    user = models.ForeignKey(LaraUser, related_name="%(app_label)s_%(class)s_user_related",
                             related_query_name="%(app_label)s_%(class)s_user_related_query", on_delete=models.CASCADE, null=True, blank=True,
                             help_text="LaraUser who created the selection")
    selected_items = models.ManyToManyField(
        Project, related_name="%(app_label)s_%(class)s_selected_items_related",
        related_query_name="%(app_label)s_%(class)s_selected_items_related_query", blank=True, help_text="selected projects")

    def __str__(self):
        return self.name or ''

    def __repr__(self) -> str:
        return self.name or ''

# ____________ calendars / planning _________


class ProjectsCalendar(CalendarAbstract):
    """_Projects Calendar_

    :param CalendarAbstract: _description_
    :type CalendarAbstract: _type_
    """
    calendar_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    user = models.ForeignKey(LaraUser, related_name="%(app_label)s_%(class)s_user_related",
                             on_delete=models.CASCADE, null=True, blank=True,  help_text="LARA User, who created the calendar entry")
    attendees = models.ManyToManyField(LaraUser, related_name='%(app_label)s_%(class)s_attendees_related',
                                       related_query_name="%(app_label)s_%(class)s_attendees_related_query", blank=True,
                                       help_text="attendees of the meeting")
    project = models.ForeignKey(Project, related_name="%(app_label)s_%(class)s_device_related",
                                on_delete=models.CASCADE, null=True, blank=True,  help_text="project to be scheduled")

    # adding dates / metadata/ ... attendees upon save (from project)
    def __str__(self):
        return self.title or ''

    def __repr__(self) -> str:
        return self.title or ''


class ExperimentCalendar(CalendarAbstract):
    """_Projects Calendar_

    :param CalendarAbstract: _description_
    :type CalendarAbstract: _type_
    """
    calendar_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    user = models.ForeignKey(LaraUser, related_name="%(app_label)s_%(class)s_user_related",
                             on_delete=models.CASCADE, null=True, blank=True,  help_text="LARA User, who created the calendar entry")
    attendees = models.ManyToManyField(LaraUser, related_name='%(app_label)s_%(class)s_attendees_related',
                                       related_query_name="%(app_label)s_%(class)s_attendees_related_query", blank=True,
                                       help_text="attendees of the meeting")
    experiment = models.ForeignKey(Experiment, related_name="%(app_label)s_%(class)s_experiment_related",
                                   on_delete=models.CASCADE, null=True, blank=True,  help_text="experiment to be scheduled")

    # adding dates / metadata/ ... attendees upon save (from project)

    def __str__(self):
        return self.title or ''

    def __repr__(self) -> str:
        return self.title or ''


# ____________ reports ______________________


class OutputTemplate(models.Model):
    """ template for generating reports from experiments/projects"""
    template_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    namespace = models.ForeignKey(Namespace, related_name="%(app_label)s_%(class)s_namespace_related",
                                  on_delete=models.CASCADE, null=True, blank=True,  help_text="namespace of the report template")
    name = models.TextField(
        unique=True, help_text="report template name")
    description = models.TextField(
        blank=True, help_text="report template description")
    format = models.TextField(
        blank=True, help_text="report template format (markdown/django, LaTeX, XML, odt, tar.bz2,  ....)")
    template_text = models.TextField(
        blank=True, help_text="report template - in plain text format, e.g. markdown/django/jinja, LaTeX ")
    file = models.FileField(
        upload_to='projects', blank=True, null=True, help_text="rel. path/filename")
    datetime_created = models.DateTimeField(default=timezone.now,
                                            help_text="creation date of the report")
    datetime_updated = models.DateTimeField(default=timezone.now,
                                            help_text="updated date of the report")
    version = models.TextField(
        blank=True, help_text="version of the report template - best in SemVer syntax")

    def __str__(self):
        return self.name or ''

    def __repr__(self) -> str:
        return self.name or ''


class Report(models.Model):
    report_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    name = models.TextField(
        unique=True, help_text="report name")
    IRI = models.URLField(
        blank=True,  null=True, unique=True, max_length=512, help_text="International Resource Identifier - IRI: is used for semantic representation ")
    handle = models.URLField(
        blank=True, null=True,  unique=True, max_length=512, help_text=" [handle URI](https://www.handle.net/)")
    description = models.TextField(
        blank=True, help_text="report description")
    template = models.ForeignKey(OutputTemplate, related_name='%(app_label)s_%(class)s_template_related',
                                 related_query_name="%(app_label)s_%(class)s_template_related_query", on_delete=models.CASCADE, null=True, blank=True,
                                 help_text="Template used for report generation")
    format = models.TextField(
        blank=True, help_text="report format MIME/type")
    text = models.TextField(
        blank=True, help_text="Actual report in plain text format")
    file = models.FileField(
        upload_to='projects/reports', blank=True, null=True, help_text="report filename with rel. path/filename")
    PDF = models.FileField(
        upload_to='projects/reports', blank=True, null=True, help_text="report PDF with rel. path/filename")

    datetime_created = models.DateTimeField(default=timezone.now,
                                            help_text="creation date of the report")
    datetime_updated = models.DateTimeField(default=timezone.now,
                                            help_text="update date of the report")
    authors = models.ManyToManyField(
        Entity, related_name="%(app_label)s_%(class)s_authors_related",
        related_query_name="%(app_label)s_%(class)s_authors_related_query", blank=True, help_text="project designers")

    def __str__(self):
        return self.name or ''

    def __repr__(self) -> str:
        return self.name or ''


# __________ Exchange / Synchronisation _______

class ProjectSynchronisationSettings(models.Model):
    """Synchronisation settings for LARA projects synchronisation with other LARA systems

    :param models: _description_
    :type models: _type_
    """

    sync_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    name = models.TextField(blank=True, help_text="name of the synchronisaton")
    target_server = models.URLField(
        blank=True, help_text="Universal Resource Locator - URL")
    login = models.TextField(
        blank=True, help_text="name of the synchronisaton")
    pass_wd = models.TextField(
        blank=True, help_text="name of the synchronisaton")
    auth_key = models.TextField(
        blank=True, help_text="name of the synchronisaton")
    cert = models.TextField(blank=True, help_text="name of the synchronisaton")
    projects = models.ManyToManyField(Project, related_name='%(app_label)s_%(class)s_projects_related',
                                      related_query_name="%(app_label)s_%(class)s_projects_related_query", blank=True,
                                      help_text="projects")
    experiment = models.ManyToManyField(Experiment, related_name='%(app_label)s_%(class)s_experiments_related',
                                        related_query_name="%(app_label)s_%(class)s_experiments_related_query", blank=True,
                                        help_text="experiments")
    datetime_started = models.DateTimeField(default=timezone.now,
                                            help_text="synchronisation start")
    duration = models.PositiveIntegerField(
        blank=True, help_text="duration of the synchronisation in seconds")
    event_repeat = models.TextField(
        blank=True, null=True, help_text="apache airflow ('@daily', '@weekly') or cron expression like entry: '12 23 * * *' :  every day at 23:12h do this task")
    event_repeat_JSON = models.JSONField(
        blank=True, null=True, help_text="advanced repeat expressions possible, e.g. for uneaven orrurences")
    logs = models.JSONField(
        blank=True, help_text="logs of the synchronisation")
    #state = choices
    description = models.TextField(
        blank=True, null=True, help_text="description of extra data type")

    def __str__(self):
        return self.name or ''

    def __repr__(self) -> str:
        return self.name or ''
